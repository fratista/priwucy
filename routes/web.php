<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

/*
| Default redirect after login
|
*/
Route::get('/home', 'HomeController@index')->name('home');


/*
| Page Routes
|
*/
Route::get('/', function () {
    return view('pages.home');
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

Route::get('/imprint', function () {
    return view('pages.imprint');
});

Route::get('/privacy', function () {
    return view('pages.privacy');
});



Route::get('/terminology', function () {
    return view('compliance.resources.terminology');
});

/*
| Grouped Compliance Routes
|
*/
Route::group(
    [
        'prefix' => 'assessments',
        'middleware' => ['auth', 'verified']
    ],
    function () {
        Route::get('/overview', 'AssessmentController@overview');
        Route::get('/create', 'AssessmentController@createAssessment');
        Route::post('/delete', 'AssessmentController@deleteAssessment');

        Route::group(
            [
                'prefix' => '{assessment}'
            ],
            function () {
                Route::get('/preliminary-check', 'AssessmentController@renderPreliminaryCheck')->name('assessment');
                Route::get('/dashboard', 'AssessmentController@renderDashboard')->name('dashboard');
                Route::get('/chapters/{chapter}', 'ChapterController@renderChapter');
               
                Route::get('/chapters/{chapter}/answered-duty-count', 'ChapterController@getAnsweredDutyCount');
                Route::get('/chapters/{chapter}/answered-feature-count', 'ChapterController@getAnsweredFeatureCount');
                Route::get('/chapters/{chapter}/answered-dispensation-count', 'ChapterController@getAnsweredDispensationCount');
                       
                Route::get('/files/overview', 'FileController@overview');
                Route::get('/files/{file}', 'ReportController@file');

                Route::get('/report', 'ReportController@renderReport')->name('report');                
            }
        );

        Route::post('/preliminary-check/update/', 'AssessmentController@updatePreliminaryCheck');
        Route::post('/preliminary-check/submit', 'AssessmentController@submitPreliminaryCheck');
        Route::post('/preliminary-check/reset', 'AssessmentController@resetPreliminaryCheck');

        Route::post('/chapter/update', 'ChapterController@updateChapter');
        Route::post('/chapter/submit', 'ChapterController@submitChapter');
        Route::post('/chapter/reset', 'ChapterController@resetChapter');

        Route::post('/chapter/duty/save', 'DutyController@saveAnswer');

        Route::post('/chapter/feature/save', 'FeatureController@saveAnswer');
        Route::post('/chapter/feature/delete', 'FeatureController@deleteAnswers');

        Route::post('/chapter/dispensation/save', 'DispensationController@saveAnswer');
        Route::post('/chapter/dispensation/delete', 'DispensationController@deleteAnswers');

        Route::post('/report/download', 'ReportController@downloadReport');
    }
);

Route::get('/tasks/overview', 'TaskController@overview');
Route::get('/tasks/task', 'TaskController@task');

/*
| Setup for the regulation data
|
*/
Route::get('/admin/reset', 'RegulationController@dropRegulation')->middleware('auth');
Route::get('/admin/setup', 'RegulationController@index')->middleware('auth');
