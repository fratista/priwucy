/* hide loading screen */
function onReady(callback) {
  	var intervalID = window.setInterval(checkReady, 0);

  	function checkReady() {
  		if ($('body') !== undefined) {
    		window.clearInterval(intervalID);
      	callback.call(this);
    	}
    }
}

function show(selector, value) {
    if (value) $(selector).addClass('in');
	else $(selector).removeClass('in');
}

/* display individual loading messages */
function displayMessage(modal, message, steps) {
    $(modal + ' .pri-loading-message').html(message);
}

/**
 * Prepare the modal windows and fill it with the right content
 *
 *
 */
function confirmationModal(title, body, btn, action) {
    $('.modal-title').html(title);
    $('.modal-body p').html(body);
    $('.modal-footer button').html(btn);
    $('.modal-footer button').attr('data-action', action).attr('id'
    , action);
    $('.modal.confirm').addClass('in');
}

function loadingModal(message) {
    $('.modal').removeClass('in');
    $('.modal.loading .pri-loading-message').html(message);
    $('.modal.loading').addClass('in');

    setTimeout(function() {
        $('.modal').removeClass('in');
    }, 1000);
}

/**
 * Provide additional frontend functionality to the authentication views
 *
 *
 */
 function authenticationHandler() {
     $('.form-control').each(function() {
         if ($(this).val().length == 1)
            $(this).next().addClass('active');
     });

     $('.form-control').on('click', function() {
         $(this).next().addClass('active');
     });

     $('form-control').on('mouseleave', function() {
         if ($(this).val().length == 0)
            $(this).next().removeClass('active');
     });

    //$(this).hasClass('active');


}

$(document).ready(function() {
    /*Navigationselemente togglen*/
	$(".logo, .navbutton, .footernav, .social-nav .downloads, .social-nav .anfahrt").not(".navbutton.login, .navbutton.cart-management, .navbutton.logout, .footernav.login").click(function(e) {
	    e.preventDefault();

		var navbuttonClass = $(this).attr('class').split(' ')[1];
		var customURL = navbuttonClass + '.php';
    });

    authenticationHandler();

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(window).on('scroll', function() {
        var offset = $('body').scrollTop();

        if (offset > 0) $(".navbar").addClass('scrolled');
        else $(".navbar").removeClass('scrolled');
    });

    $('.modal .close').on('click', function() {
        $('.modal').removeClass('in');
    });

    $('#accept-cookie').on('click', function(e) {
        e.preventDefault();
        $('.cookie-bar').addClass('gone');

        var nDays = 30;
        var cookieName = "disclaimer";
        var cookieValue = "true";

        var today = new Date();
        var expire = new Date();
        expire.setTime(today.getTime() + 3600000*24*nDays);
        document.cookie = cookieName+"="+escape(cookieValue)+";expires="+expire.toGMTString()+";path=/";
     });

     $('.control-label').on('click', function() {
         $(this).toggleClass('active');
     });
});
