/**
 * Autosave the answers of the preliminary check to the assessment model
 *
 *
 */
$('body').off('click', '.radio-preliminary').on('click', '.radio-preliminary', function() {
    var name = $(this).attr('name');
    var id = $(this).data('id');
    var option = name.substring(6, 7);

    var parent_class = $(this).parents().eq(1).attr('class');

    if (parent_class.indexOf('multiple') > -1) {
        //if (parseInt(option) > 10) console.log ("XXX");
        var opposite = (option == 'y') ? 'n' : 'y';
        var iteration = name.substring(8, 9);

        var check = '[name="radio-' + option + '-' + iteration + '-' + id + '"]';
        var uncheck = '[name="radio-' + opposite + '-' + iteration + '-' + id + '"]';

        $(check).prop('checked', true).addClass('checked');
        $(uncheck).prop('checked', false).removeClass('checked');

        var question_count = 0;

        $(this).parents().eq(2).find('.radio-preliminary').each(function() {
            question_count++;
        });

        question_count = question_count / 2;

        var answer_count = 0;
        var answer = '';

        $(this).parents().eq(2).find('.radio-preliminary.checked').each(function() {
            answer += $(this).attr('value') + ';';
            answer_count++;
        });

        // add 0 values to unanswered questions to avoid exception if now answer is given
        if (answer_count < question_count) {
            for (var j = 0; j < (question_count - answer_count); j++) {
                answer += '0;';
            }
        }
    } else {
        var opposite = (option == 'y') ? 'n' : 'y';

        var check = '[name="radio-' + option + '-' + id + '"]';
        var uncheck = '[name="radio-' + opposite + '-' + id + '"]';

        $(check).prop('checked', true).addClass('checked');
        $(uncheck).prop('checked', false).removeClass('checked');

        var answer = $(this).attr('value');
    }

    $.ajax({
        url: '/assessments/preliminary-check/update',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            id: id,
            answer: answer
        }
    });
});

/**
 * Submit the answers of the preliminary check to the assessment model
 *
 *
 */
$('body').off('submit', '#form-preliminary').on('submit', '#form-preliminary', function(e) {
    e.preventDefault();

    var item_count = 0;
    var answer_count = 0;

    $(".item-container").each(function() {
        var items = $(this).find('input[type=radio]').val();
        if (items) item_count++;

        var answer = $(this).find('input[type=radio]:checked').val();
        if (answer) answer_count++;

    });
    console.log();

    if (item_count != answer_count) {
        loadingModal('All questions have to be answered');

        return;
    }

    confirmationModal(
        'Do you want to submit the preliminary check?',
        'By doing so, your answers will be locked',
        'Submit Preliminary Check',
        'modal-submit-preliminary'
    );

    $('.modal').off('click', '#modal-submit-preliminary').on('click', '#modal-submit-preliminary', function() {
        loadingModal('Submitting answers. You will be redirected to the dashboard shortly...');

        $.ajax({
            url: '/assessments/preliminary-check/submit',
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function(data) {
                setTimeout(function() {
                    window.location.href = 'dashboard';
                }, 500);
            }
        });
    });
});

/**
 * Reset the answers of the preliminary check and therefore the complete assessment
 *
 *
 */
$('body').off('click', '#reset-preliminary').on('click', '#reset-preliminary', function(e) {
    e.preventDefault();

    confirmationModal(
        'Do you want to reset the preliminary check?',
        'You have already answered the following questions. Please press the rest button in order to change the answers. Pressing reeset will also reset the GDPR dashboard',
        'Reset Preliminary Check',
        'modal-reset-preliminary'
    );

    $('.modal').on('click', '#modal-reset-preliminary', function() {
        loadingModal('Resetting answers and initialising survey...');

        $.ajax({
            url: '/assessments/preliminary-check/reset',
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            success: function(data) {
                setTimeout(function() {
                    window.location.href = 'assessments/assessment/preliminary-check';
                }, 500);
            }
        });
    });
});

/**
 * Save the duty answers to the corresponding table and lock eventual duplicates
 * in the assessment to avoid unintended change of answers which have been provided
 * on different positions in the assessment.
 *
 *
 */
$('body').off('click', '.duty-radio').on('click', '.duty-radio', function() {
    var id = '#' + $(this).attr('id');
    var answer = parseInt($(this).val());
    var comment = $(this).parents(':eq(3)').find('.duty-comment').val();
    var attachment = {};
    var duty_uid = parseInt($(this).parents(':eq(3)').find('[data-duty-id]').data('duty-id'));

    $.ajax({
        url: '/assessments/chapter/duty/save',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            answer: answer,
            comment: comment,
            attachment: attachment,
            duty_id: duty_uid
        },
        success: function(data) {
            switch (answer) {
                case 1:
                case 2:
                    $(id).parents(':eq(3)').find('.item-dependencies .feature-container').removeClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .related-feature-container').addClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .dispensation-container').addClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .dispensation-feature-container').addClass('collapse');

                    /**
                     * Delete the related duty dependencies
                     */
                    $.ajax({
                        url: '/assessments/chapter/dispensation/delete',
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            duty_id: duty_uid
                        },
                        success: function(data) {
                            $(id).parents(':eq(3)').find('.dispensation-radio').removeAttr('checked');
                            $(id).parents(':eq(3)').find('.dispensation-radio + .outer .inner').css('opacity', 0);
                            $(id).parents(':eq(3)').find('.dispensation-radio + .outer').css('border-color', '#0c70b4');

                            $.ajax({
                                url: '/assessments/chapter/feature/delete',
                                type: 'post',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                dataType: 'json',
                                data: {
                                    parent_type: 'dispensation_feature',
                                    grandparent_id: duty_uid
                                },
                                success: function(data) {
                                    $(id).parents(':eq(3)').find('.dispensation-feature-radio').removeAttr('checked');
                                    $(id).parents(':eq(3)').find('.dispensation-feature-radio + .outer .inner').css('opacity', 0);
                                    $(id).parents(':eq(3)').find('.dispensation-feature-radio + .outer').css('border-color', '#0c70b4');
                                }
                            });
                        }
                    });

                    break;
                case 0:
                    $(id).parents(':eq(3)').find('.item-dependencies .dispensation-container').removeClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .feature-container').addClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .related-feature-container').addClass('collapse');
                    $(id).parents(':eq(3)').find('.item-dependencies .dispensation-feature-container').addClass('collapse');

                    $.ajax({
                        url: '/assessments/chapter/feature/delete',
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            parent_type: 'duty',
                            parent_id: duty_uid
                        },
                        success: function(data) {
                            $(id).parents(':eq(3)').find('.feature-radio').removeAttr('checked');
                            $(id).parents(':eq(3)').find('.feature-radio + .outer .inner').css('opacity', 0);
                            $(id).parents(':eq(3)').find('.feature-radio + .outer').css('border-color', '#0c70b4');

                            $.ajax({
                                url: '/assessments/chapter/feature/delete',
                                type: 'post',
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                dataType: 'json',
                                data: {
                                    parent_type: 'related_feature',
                                    parent_id: duty_uid
                                },
                                success: function(data) {
                                    $(id).parents(':eq(3)').find('.related-feature-radio').removeAttr('checked');
                                    $(id).parents(':eq(3)').find('.related-feature-radio + .outer .inner').css('opacity', 0);
                                    $(id).parents(':eq(3)').find('.related-feature-radio + .outer').css('border-color', '#0c70b4');
                                }
                            });
                        }
                    });

                    break;
            }

            updateChapterProgress();
        }
    });
});

/**
 * Save the associated dispensations for the selected duty
 *
 *
 */
$('body').off('click', '.dispensation-radio').on('click', '.dispensation-radio', function() {
    var elem = this;
    var class_selector = '.' + $(this).attr('class').substring(19);

    var answer = parseInt($(this).val());
    var comment = $(this).parents(':eq(3)').find('.dispensation-comment').val();
    var attachment = {};
    var dispensation_id = $(this).parents(':eq(3)').find('[data-dispensation-id]').data('dispensation-id');
    var duty_id = $(this).parents(':eq(5)').find('[data-duty-id]').data('duty-id');

    $(this).next().find('.inner').css('opacity', 1);
    $(this).next().css('border-color', '#f08b3b');

    $.ajax({
        url: '/assessments/chapter/dispensation/save',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            answer: answer,
            comment: comment,
            attachment: attachment,
            dispensation_id: dispensation_id,
            duty_id: duty_id
        },
        success: function(data) {
            $(class_selector).each(function() {
                $(this).prop('checked', true);
                $(this).parents(':eq(3)').closest('.item-container').addClass('related');
                $(this).parents(':eq(4)').find('.item-relation span').html('This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.');
                $(this).parents(':eq(3)').find('.item-answers').addClass('pri-cursor');
                $(this).parents(':eq(3)').find('textarea, .input-group').addClass('pri-disabled');
                $(this).parents(':eq(1)').find('.radio-label').addClass('pri-disabled');
            });

            $(elem).parents(':eq(3)').closest('.item-container').removeClass('related');
            $(elem).parents(':eq(4)').find('.item-relation span').html('');
            $(elem).parents(':eq(3)').find('.item-answers').removeClass('pri-cursor');
            $(elem).parents(':eq(3)').find('textarea, .input-group').removeClass('pri-disabled');
            $(elem).parents(':eq(1)').find('.radio-label').removeClass('pri-disabled');

            if (answer == 1) {
                $(class_selector).each(function() {
                    $(this).parents(':eq(3)').find('.item-dependencies .item-container').removeClass('collapse');
                });

                updateChapterProgress();
            } else {
                $(class_selector).each(function() {
                    $(this).parents(':eq(3)').find('.item-dependencies .item-container').addClass('collapse');
                });

                /**
                 * Delete the related dispensation features by answering with no
                 */
                $.ajax({
                    url: '/assessments/chapter/feature/delete',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        dispensation_id: dispensation_id,
                        dispensation_duty_id: duty_id
                    },
                    success: function(data) {
                        $(elem).parents(':eq(3)').find('.dispensation-radio + .outer .inner').css('opacity', 0);
                        $(elem).parents(':eq(3)').find('.dispensation-radio + .outer').css('border-color', '#0c70b4');

                        updateChapterProgress();
                    }
                });
            }
        }
    });
});

/**
 * Save the associated features for the selected duty, feature or dispensation
 *
 *
 */
$('body').off('click', '.feature-radio').on('click', '.feature-radio', function() {
    var elem = this;
    var class_selector = '.' + $(this).attr('class').substring(14);

    var answer = parseInt($(this).val());

    var grandparent_id = $(this).parents(':eq(9)').find('[data-duty-id]').data('duty-id');

    if ($(this).hasClass('related-feature-radio')) {
        var comment = $(this).parents(':eq(3)').find('.related-feature-comment').val();
        var attachment = {};
        var feature_id = $(this).parents(':eq(3)').find('[data-related-feature-id]').data('related-feature-id');

        var parent_type = 'feature';
        var parent_id = $(this).parents(':eq(5)').find('[data-feature-id]').data('feature-id');
    } else if ($(this).hasClass('dispensation-feature-radio')) {
        var comment = $(this).parents(':eq(3)').find('.dispensation-feature-comment').val();
        var attachment = {};
        var feature_id = $(this).parents(':eq(3)').find('[data-dispensation-feature-id]').data('dispensation-feature-id');

        var parent_type = 'dispensation';
        var parent_id = $(this).parents(':eq(5)').find('[data-dispensation-id]').data('dispensation-id');
    } else {
        var comment = $(this).parents(':eq(3)').find('.feature-comment').val();
        var attachment = {};
        var feature_id = $(this).parents(':eq(3)').find('[data-feature-id]').data('feature-id');

        var parent_type = 'duty';
        var parent_id = $(this).parents(':eq(5)').find('[data-duty-id]').data('duty-id');
        var grandparent_id = null;
    }

    $(this).next().find('.inner').css('opacity', 1);
    $(this).next().css('border-color', '#f08b3b');

    $.ajax({
        url: '/assessments/chapter/feature/save',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            answer: answer,
            comment: comment,
            attachment: attachment,
            feature_id: feature_id,
            parent_type: parent_type,
            parent_id: parent_id,
            grandparent_id: grandparent_id
        },
        success: function(data) {
            $(class_selector).each(function() {
                $(this).prop('checked', true);
                $(this).parents(':eq(3)').closest('.item-container').addClass('related');
                $(this).parents(':eq(4)').find('.item-relation span').html('This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.');
                $(this).parents(':eq(3)').find('.item-answers').addClass('pri-cursor');
                $(this).parents(':eq(3)').find('textarea, .input-group').addClass('pri-disabled');
                $(this).parents(':eq(1)').find('.radio-label').addClass('pri-disabled');
            });

            $(elem).parents(':eq(3)').closest('.item-container').removeClass('related');
            $(elem).parents(':eq(4)').find('.item-relation span').html('');
            $(elem).parents(':eq(3)').find('.item-answers').removeClass('pri-cursor');
            $(elem).parents(':eq(3)').find('textarea, .input-group').removeClass('pri-disabled');
            $(elem).parents(':eq(1)').find('.radio-label').removeClass('pri-disabled');

            if (answer == 1) {
                $(class_selector).each(function() {
                    $(this).parents(':eq(3)').find('.item-dependencies .item-container').removeClass('collapse');
                });

                updateChapterProgress();
            } else {
                $(class_selector).each(function() {
                    $(this).parents(':eq(3)').find('.item-dependencies .item-container').addClass('collapse');
                });

                /**
                 * Delete the related features of a feature. In the other two cases this is not necessary.
                 *
                 */
                $.ajax({
                    url: '/assessments/chapter/feature/delete',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    data: {
                        parent_type: 'related_feature',
                        parent_id: parent_id
                    },
                    success: function(data) {
                        $(elem).parents(':eq(3)').find('.related-feature-radio').removeAttr('checked');
                        $(elem).parents(':eq(3)').find('.related-feature-radio + .outer .inner').css('opacity', 0);
                        $(elem).parents(':eq(3)').find('.related-feature-radio + .outer').css('border-color', '#0c70b4');

                        updateChapterProgress();
                    }
                });
            }
        }
    });
});

/**
 * Submit the chapter and lockdown the questions
 *
 *
 */

$('body').off('click', '#submit-chapter').on('click', '#submit-chapter', function(e) {
    e.preventDefault();

    var status = updateChapterProgress();

    submitChapter(status);
});

/**
 * Reset the chapter and enable the questions
 *
 *
 */

$('body').off('click', '.reset-chapter').on('click', '.reset-chapter', function(e) {
    e.preventDefault();

    resetChapter();
});
















/***********************************
 ********toggle report details*******
 ***********************************/

var offsetOpen;
var offsetClose;

$('body').on('click', '#view-details', function() {
    offsetOpen = 180;

    $('html, body').animate({
        scrollTop: offsetOpen
    }, 'slow');

    offsetClose = $(this).parents().eq(4).offset().top - 100;

    /***********************************
    show focus container and display content inside focus container on top
    ***********************************/
    $('.focus-wrapper .chapter-focus-body').empty();
    $('.focus-wrapper').removeClass('report-hidden');

    toggleOverview();

    expandChapter();

    var chapterContent = $(this).parents().eq(3).clone();
    chapterContent.appendTo($(".chapter-focus-body"));

    toggleOverview(true);

    $('.chapter-focus-body .chapter-section').addClass('in');
    $('.chapter-focus-body .chapter-detail').addClass('in');
    $('.chapter-focus-body .chapter-print').addClass('in');
    $('.chapter-focus-body #view-details').remove();

    $('.chapter-section').addClass('focus');
});



$('body').on('click', '#hide-details', function() {
    $('.focus-wrapper').addClass('report-hidden');

    toggleOverview(true);
    //$('.chapter-container').removeClass('col-md-6').addClass('col-sm-8 col-md-6 col-lg-4 col-xl-3');

    $('html, body').animate({
        scrollTop: offsetClose
    }, 'slow');
});


/*
 * expand focussed duties
 *
 * 
 */

$('body').on('click', '#expand-fulfilled, #expand-unfulfilled, #expand-unsure', function() {
    $(this).parents().eq(1).find('.report-item-container').toggleClass('report-hidden');
});

/***********************************
 *******toggle extended view*********
 ***********************************/
$('.pri-toggle-checkbox').on('change', function() {
    var initialLayout = 'col-sm-8 col-md-6 col-lg-4 col-xl-3';
    var checked = $(this).attr('checked');

    if (checked !== 'checked') {
        $(this).attr('checked', true);

        $('.chapter-container').removeClass(initialLayout).addClass('col-md-6');
        $('.chapter-total').addClass('collapse');
        $('.chapter-split').removeClass('collapse');
    } else {
        $(this).attr('checked', false);

        $('.chapter-container').removeClass('col-md-6').addClass(initialLayout);
        $('.chapter-total').removeClass('collapse');
        $('.chapter-split').addClass('collapse');
    }
});



/*
 * Report Download Selection
 *
 *
 */

$('body').off('click', '[data-action=generate-report]').on('click', '[data-action=generate-report]', function(e) {
    e.preventDefault();

    $('[data-action=cancel-report], [data-action=select-all-report]').removeClass('d-none').addClass('d-inline-block');
    $('[data-action=generate-report]').removeClass('d-inline-block').addClass('d-none');

    $('.chapter-wrapper').addClass('selectable');

    $('body').off('click', '.chapter-wrapper.selectable .chapter-section').on('click', '.chapter-wrapper.selectable .chapter-section', function() {
        $(this).toggleClass('selected');

        var selectedCount = $('.chapter-section.selected').length;

        if (selectedCount) {
            $('[data-action=download-report]').removeClass('d-none').addClass('d-inline-block');

            $('body').off('click', '[data-action=download-report]').on('click', '[data-action=download-report]', function() {
                var chapters = [];
                var chapter_content = '';

                $.each($('.chapter-section.selected'), function() {
                    chapter_id = $(this).data('chapter-name');
                    chapters.push(chapter_id);
                });

                $.each(chapters, function(k, v) {
                    chapter_content += $('[data-chapter-name=' + v + ']').html();
                });

                var file_name = $('.dropdown-toggle').text();

                var data = {
                    chapter_content: chapter_content,
                    file_name: file_name
                };

                $.ajax({
                    url: '/assessments/report/download-report',
                    type: 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    data: data,
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
        } else {
            $('[data-action=download-report]').addClass('d-none').removeClass('d-inline-block');
            $('body').off('click', '[data-action=download-report]');
        }
    });

    $('[data-action=cancel-report]').on('click', function() {
        $('.chapter-wrapper').removeClass('selectable');
        $('.chapter-section').removeClass('selected');

        $('[data-action=cancel-report], [data-action=select-all-report], [data-action=download-report]').removeClass('d-inline-block').addClass('d-none');
        $('[data-action=generate-report]').removeClass('d-none').addClass('d-inline-block');
    });

    $('[data-action=select-all-report]').on('click', function() {
        $('.chapter-section').addClass('selected');


        /* make reusable function()!! */
        $('[data-action=download-report]').removeClass('d-none').addClass('d-inline-block');
    });
});

function toggleOverview(expansion) {
    var base_layout = 'col-sm-8 col-md-6 col-lg-4 offset-sm-2 offset-md-0';
    var checked = $('.pri-toggle-checkbox').attr('checked');

    if (expansion) {
        if (checked == 'checked') {
            $('.chapter-container').removeClass(base_layout).addClass('col-md-6');
            expandChapter();
        } else {
            $('.chapter-container').removeClass('col-md-6').addClass(base_layout);
            collapseChapter();
        }
    } else {
        if (checked !== 'checked') {
            $('.chapter-container').removeClass(base_layout).addClass('col-md-6');
            collapseChapter();
        } else {
            $('.chapter-container').removeClass('col-md-6').addClass(base_layout);
            expandChapter();
        }
    }
}

/*
 * Toggle the chapter detail view on the report page.
 *
 *
 */
function expandChapter() {
    $('.chapter-wrapper .chapter-total').addClass('collapse');
    $('.chapter-wrapper .chapter-split').removeClass('collapse');
}

function collapseChapter() {
    $('.chapter-wrapper .chapter-total').removeClass('collapse');
    $('.chapter-wrapper .chapter-split').addClass('collapse');
}

/*
 * Update the chapter progress when selecting answers. The progress can be dynamic when new questions become available.
 *
 *
 */
function updateChapterProgress() {
    var chapter_id = $('input[name=chapter-id]').val();

    var item_count = 0;
    var answer_count = 0;

    $('.item-container:not(.collapse):not(.related)').each(function() {
        item_count++;
    });

    $('.item-container:not(.collapse):not(.related)').each(function() {
        var answer = $(this).find('input[type=radio]:checked').val();
        if (answer) answer_count++;
    });

    var status = (answer_count / item_count) * 100;

    $.ajax({
        url: '/assessments/chapter/update',
        type: 'post',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        data: {
            chapter_id: chapter_id,
            status: status
        },
        success: function(data) {
            console.log(data);
        }
    });
    
    return status;    
}

function submitChapter(status) {
    $('.modal-title').html('Do you want to submit this chapter?');
    $('.modal-body p').html('By doing so, your answers will be locked');
    $('.modal-footer .btn').html('Submit Chapter');
    $('.modal-footer .btn').attr('id', 'modal-submit-chapter');
    $('.modal.confirm').addClass('in');

    if (status == 100.00) {
        $('.modal').off('click', '#modal-submit-chapter').on('click', '#modal-submit-chapter', function() {
            var chapter_id = $('input[name=chapter-id]').val();

            $('.modal.confirm').removeClass('in');
            $('.pri-loading-message').html('Submitting your answers');
            $('.modal.loading').addClass('in');

            $.ajax({
                url: '/assessments/chapter/submit',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    chapter_id: chapter_id
                },
                success: function(data) {
                    window.setTimeout(function() {
                        window.location.href = '/assessments/assessment/dashboard';
                    }, 500);
                }
            });
        });
    } else {
        $('.modal.confirm').removeClass('in');
        $('.pri-loading-message').html('Please answer all questions');
        $('.modal.loading').addClass('in');
        
        setTimeout(function() {
            $('.modal.loading').removeClass('in');
        }, 1000);
    }
}

function resetChapter() {
    var chapter_id = $('input[name=chapter-id]').val();

    $('.modal-title').html('Do you want to reset this chapter?');
    $('.modal-body p').html('This will reset the progress of this chapter');
    $('.modal-footer .btn').html('Reset Chapter');
    $('.modal-footer .btn').attr('id', 'modal-reset-chapter');
    $('.modal.confirm').addClass('in');

    $('.modal').off('click', '#modal-reset-chapter').on('click', '#modal-reset-chapter', function() {
        $('.modal.confirm').removeClass('in');
        $('.pri-loading-message').html('Resetting chapter ...');
        $('.modal.loading').addClass('in');

        $.ajax({
            url: '/assessments/chapter/reset',
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            data: {
                chapter_id: chapter_id
            },
            success: function(data) {
                window.setTimeout(function() {
                    location.reload();
                }, 500);
            }
        });
    });
}