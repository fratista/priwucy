function initializeTerminology() {        
    $('.language-option').on('change', function() {
        var language = $( ".language-option option:selected" ).val();  

        $.ajax({
            url: ajaxHandler,
            type: 'post',
            data: { 
                appPath : true,
                class : 'Application',
                setLanguage : true,
                language : language
            },
            success: function(data) {
                location.reload();
                //window.location.href = 'assessment.php?view=definitions';
            }
        });
    });
    
    var language;
    var articleId;
    
    $.ajax({
        url: ajaxHandler,
        type: 'post',
        data: { 
            appPath : true,
            class : 'Application',
            getLanguage : true
        },
        success: function(data) {
            language = data;
            language = language.replace(/(\r\n|\n|\r)/gm,""); 
            language = language.replace(/ /g,'');
            type = 'post';  

            var domain = 'http://eur-lex.europa.eu/legal-content/' + language + '/TXT/HTML/?uri=CELEX:32016R0679';

            $.ajax({
                url: ajaxHandler,
                type: 'post',
                data: {
                    appPath : true,
                    class : 'Terminology',
                    definitionId : true
                },
                success: function(data) {
                    articleId = data;   
                    
                    $.ajax({
                        url: ajaxHandler,
                        type: 'post',
                        data: { 
                            appPath : true,
                            class : 'Terminology',
                            curlSite : true,
                            domain : domain,
                            type : type
                        },  
                        success: function(data) { 
                            data = data.replace(/.*/, "").substr(53);

                            xml = $.parseXML(data);

                            $xml = $(xml);                 

                            var e = $xml.find('#d1e1489-1-1');

                            console.log(xml);

                            var test = $xml.find('#'+articleId).next().html();  

                    
                    //var table = $xml.find("#"+articleId).nextAll().eq(2).html();
                    //console.log(test); 
                    
                    //var paragraph = $(table).find('.normal').last().html();
 //console.log(paragraph);
                    //x = data.substring(data.indexOf("\n") + 1);



                   // var article = $(data).find('#d1e1489-1-1').html();
                    //console.log($(x).find('#d1e1489-1-1').length);
                    //console.log($("#d1e1489-1-1", x).length);

                    //console.log(data);
                    //console.log(article);
                        }
                    });                    
                }
            });            
        }
    });
}