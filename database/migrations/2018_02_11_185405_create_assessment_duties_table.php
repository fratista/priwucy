<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentDutiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_duties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('answer');
            $table->text('comment')->nullable();
            $table->string('filename')->nullable();
			$table->string('mime')->nullable();
			$table->string('original_filename')->nullable();
            $table->integer('duty_id')->unsigned();
            $table->foreign('duty_id')->references('id')->on('duties')->onDelete('cascade');
            $table->integer('assessment_id')->unsigned();
            $table->foreign('assessment_id')->references('id')->on('assessments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_duties');
    }
}
