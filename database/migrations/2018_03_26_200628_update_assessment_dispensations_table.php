<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssessmentDispensationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessment_dispensations', function (Blueprint $table) {
            $table->integer('duty_id')->unsigned()->after('dispensation_id');
            $table->foreign('duty_id')->references('id')->on('duties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessment_dispensations', function (Blueprint $table) {
            $table->dropForeign(['duty_id']);
            $table->dropColumn('duty_id');
        });
    }
}
