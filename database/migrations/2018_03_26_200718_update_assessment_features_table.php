<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAssessmentFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessment_features', function (Blueprint $table) {
            $table->integer('duty_id')->unsigned()->nullable()->after('feature_id');
            $table->foreign('duty_id')->references('id')->on('duties')->onDelete('cascade');

            $table->integer('dispensation_id')->unsigned()->nullable()->after('duty_id');
            $table->foreign('dispensation_id')->references('id')->on('dispensations')->onDelete('cascade');

            $table->integer('dispensation_duty_id')->unsigned()->nullable()->after('dispensation_id');
            $table->foreign('dispensation_duty_id')->references('id')->on('duties')->onDelete('cascade');

            $table->integer('related_feature_id')->unsigned()->nullable()->after('dispensation_duty_id');
            $table->foreign('related_feature_id')->references('id')->on('features')->onDelete('cascade');

            $table->integer('related_feature_duty_id')->unsigned()->nullable()->after('related_feature_id');
            $table->foreign('related_feature_duty_id')->references('id')->on('duties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessment_features', function (Blueprint $table) {
            $table->dropForeign(['duty_id']);
            $table->dropColumn('duty_id');

            $table->dropForeign(['dispensation_id']);
            $table->dropColumn('dispensation_id');

            $table->dropForeign(['dispensation_duty_id']);
            $table->dropColumn('dispensation_duty_id');

            $table->dropForeign(['related_feature_id']);
            $table->dropColumn('related_feature_id');

            $table->dropForeign(['related_feature_duty_id']);
            $table->dropColumn('related_feature_duty_id');
        });
    }
}
