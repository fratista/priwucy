<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensationFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensation_features', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dispensation_id')->unsigned();
            $table->foreign('dispensation_id')->references('id')->on('dispensations')->onDelete('cascade');
            $table->integer('feature_id')->unsigned()->nullable();
            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispensation_features');
    }
}
