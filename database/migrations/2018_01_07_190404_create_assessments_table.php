<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Traits\RegulationTrait;

class CreateAssessmentsTable extends Migration
{
    use RegulationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('preliminary_check');
            $table->boolean('report_ready');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
            /*
            $table->uuid('uuid')->unique();
            $table->boolean('PreliminaryCheck')->default(0);
            $table->boolean('AnonymousData')->default(0);
            $table->boolean('AutomatedProcessing')->default(0);
            $table->boolean('DPOconditions')->default(0);
            $table->boolean('DirectMarketing')->default(0);
            $table->boolean('DirectlyCollected')->default(0);
            $table->boolean('IndirectlyCollected')->default(0);
            $table->boolean('Minors')->default(0);
            $table->boolean('NecessaryLegalObligation')->default(0);
            $table->boolean('NecessaryLegitimateInterests')->default(0);
            $table->boolean('NecessaryPerformanceContract')->default(0);
            $table->boolean('NecessaryProtectVitalInterests')->default(0);
            $table->boolean('NecessaryPublicInterestTask')->default(0);
            $table->boolean('ObtainConsent')->default(0);
            $table->boolean('OutsideEU')->default(0);
            $table->boolean('PIAconditions')->default(0);
            $table->boolean('SensitiveData')->default(0);
            $table->boolean('Unconstrained')->default(0);
            $table->boolean('ReportReady')->default(0);
            $table->decimal('CodesofConduct', 5, 2)->default(0);
            $table->decimal('InfoandAccess', 5, 2)->default(0);
            $table->decimal('Obligations', 5, 2)->default(0);
            $table->decimal('PIA', 5, 2)->default(0);
            $table->decimal('Principles', 5, 2)->default(0);
            $table->decimal('ProtectionOfficer', 5, 2)->default(0);
            $table->decimal('RectificationErasure', 5, 2)->default(0);
            $table->decimal('Restrictions', 5, 2)->default(0);
            $table->decimal('RightObjectAutomated', 5, 2)->default(0);
            $table->decimal('Security', 5, 2)->default(0);
            $table->decimal('Transfers', 5, 2)->default(0);
            $table->decimal('Transparent', 5, 2)->default(0);
            $table->integer('client_id')->unique()->nullable();
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
