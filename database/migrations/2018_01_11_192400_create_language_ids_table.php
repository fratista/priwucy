<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_ids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('BG')->unique();
            $table->string('ES')->unique();
            $table->string('CS')->unique();
            $table->string('DA')->unique();
            $table->string('DE')->unique();
            $table->string('ET')->unique();
            $table->string('EL')->unique();
            $table->string('EN')->unique();
            $table->string('FR')->unique();
            $table->string('GA')->unique();
            $table->string('HR')->unique();
            $table->string('IT')->unique();
            $table->string('LV')->unique();
            $table->string('LT')->unique();
            $table->string('HU')->unique();
            $table->string('MT')->unique();
            $table->string('NL')->unique();
            $table->string('PL')->unique();
            $table->string('PT')->unique();
            $table->string('RO')->unique();
            $table->string('SK')->unique();
            $table->string('SL')->unique();
            $table->string('FI')->unique();
            $table->string('SV')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_ids');
    }
}
