<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Response;
use Session;

use Carbon\Carbon;

class Dispensation extends Model
{
    /**
     * Save the stored user anwer for a specific dispensation
     *
     * @param \Request $request
     * @return int
     */
    public function saveAnswer($request)
    {
        $attachment = [
             'filename' => 'test',
             'mime' => 'jpeg2wbmp',
             'original_filename' => ''
         ];

        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_dispensation = DB::table('assessment_dispensations')
             ->where([
                 'dispensation_id' => $request->dispensation_id,
                 'assessment_id' => $assessment_id
             ])->first();

        /**
         * Option to answer each question separately. First the query
         * checks if there is an answer with the given signature (parent_id, granparent_id).
         * If none is available it will be created in addition to the answers of the same
         * feature that were provided in different questions. This will only be done once
         * it is set in the assessment configuration.
         *
         */
        if (config('assessment.answer_mode') == 'individual') {
            $assessment_dispensation = DB::table('assessment_dispensations')
                 ->where([
                     'dispensation_id' => $request->dispensation_id,
                     'duty_id' => $request->duty_id,
                     'assessment_id' => $assessment_id
                 ])->first();
        }

        if ($assessment_dispensation) {
            DB::table('assessment_dispensations')
                 ->where([
                     'dispensation_id' => $request->dispensation_id,
                     'duty_id' => $request->duty_id,
                     'assessment_id' => $assessment_id
                 ])
                 ->update([
                     'answer' => $request->answer,
                     'comment' => $request->comment,
                     'filename' => $attachment['filename'],
                     'mime' => $attachment['mime'],
                     'original_filename' => $attachment['original_filename'],
                     'updated_at' => Carbon::now()
                 ]);

            return 2;
        } else {
            DB::table('assessment_dispensations')->insert([
                 'answer' => $request->answer,
                 'comment' => $request->comment,
                 'filename' => $attachment['filename'],
                 'mime' => $attachment['mime'],
                 'original_filename' => $attachment['original_filename'],
                 'dispensation_id' => $request->dispensation_id,
                 'duty_id' => $request->duty_id,
                 'assessment_id' => $assessment_id,
                 'created_at' => Carbon::now()
             ]);

            return 1;
        }

        return 0;
    }

    /**
     * Return the stored user anwer for a specific dispensation depending
     * on the mode selected in the assessment configuration.
     *
     * @param $dispensation_id, $duty_id
     * @return $assessment_dispensation
     */
    public static function getAnswer($dispensation_id, $duty_id = null)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_dispensation = DB::table('assessment_dispensations')
             ->where([
                 'dispensation_id' => $dispensation_id,
                 'assessment_id' => $assessment_id
             ])->first();

        if (config('assessment.answer_mode') == 'individual') {
            $assessment_dispensation = DB::table('assessment_dispensations')
                 ->where([
                     'dispensation_id' => $dispensation_id,
                     'duty_id' => $duty_id,
                     'assessment_id' => $assessment_id
                 ])->first();
        }

        if (!$assessment_dispensation) {
            $assessment_dispensation = collect();
            $assessment_dispensation->answer = 3;
            $assessment_dispensation->comment = '';
            $assessment_dispensation->original_filename = 'No file selected';
            $assessment_dispensation->duty_id = null;
        }

        return $assessment_dispensation;
    }

    /**
     * Delete the associated dependencies of answers when set to yes or unsure.
     *
     * @param @param \Request $request
     * @return int
     */
    public function deleteAnswers($request)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_dispensation = DB::table('assessment_dispensations')
              ->where([
                  'duty_id' => $request->duty_id,
                  'assessment_id' => $assessment_id
              ])->delete();
        return $request->duty_id;
        return 1;
    }
}
