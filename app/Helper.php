<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Helper extends Model
{
    public static function createGUID()
    {
        if (function_exists('com_create_guid')) {
            $GUID = com_create_guid();
        } else {
            mt_srand((double)microtime()*10000); //optional for php 4.2.0 and up.
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $uuid = chr(123)// "{"
                .substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid, 12, 4).$hyphen
                .substr($charid, 16, 4).$hyphen
                .substr($charid, 20, 12)
                .chr(125);// "}"
            $GUID = $uuid;
        }

        if (!isset($_COOKIE['GUID'])) {
            setcookie("GUID", $GUID, time() + (86400 * 365), '/');
        }
    }

    public static function extractName($uri)
    {
        $parts = explode('/', $uri);
        $name = $parts[count($parts)-1];

        return $name;
    }

    /**
     * Wrap the variable with <> for the execution of SPARQL queries.
     *
     *
     */
    public static function wrapEntity($entity)
    {
        $entity = '<' . $entity . '>';
        return $entity;
    }

    public static function menuIcons($chapter)
    {
        $icons = [
            'Principles' => 'fa-bookmark',
            'Transparency' => 'fa-cloud',
            'Information and Access' => 'fa-universal-access',
            'Rectification and Erasure' => 'fa-eraser',
            'Automated Decisions' => 'fa-object-group',
            'Restrictions' => 'fa-exclamation-triangle',
            'General Obligations' => 'fa-tasks',
            'Security' => 'fa-user-secret',
            'Impact Assessment' => 'fa-fire',
            'Data Protection Officer' => 'fa-shield',
            'Certification' => 'fa-user-circle-o',
            'Outside the EU' => 'fa-download',
        ];

        return $icons[$chapter];
    }

    public static function setLanguage($language)
    {
        $_SESSION['language'] = $language;
    }

    public static function getLanguage()
    {
        return $_SESSION['language'];
    }
}
