<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Auth;
use Response;
use Session;
use Carbon\Carbon;

class Assessment extends Model
{
    use Traits\FileUpload;

    public $preliminaryProgress = 0;
    public $dashboardStatus = 0;
    public $answers;

    /**
     * Get basic data for the assessment to provide further usage
     * in the sub-views and the side navigation. This function will be called
     * from every other controller method which will render views
     *
     * @return array
     */
    public static function initializeAssessment()
    {
        $assessment_entry = Assessment::where('user_id', Auth::id())->first();

        if (!$assessment_entry) {
            /* change to this again */
            $assessment = new Assessment();

            $assessment->name = Auth::user()->name;
            $assessment->preliminary_check = 0;
            $assessment->report_ready = 0;
            $assessment->user_id = Auth::id();
            $assessment->client_id = NULL;
            $assessment->save();
        }

        $chapters = Assessment::getChapterOverview();
        $constraints = Assessment::getPreliminaryConstraints();

        $preliminary_completion = Assessment::getPreliminaryCompletion();
        
        Assessment::setAssessmentCompletion($chapters);
        $assessment_completion = Assessment::getAssessmentCompletion();

        $disabled = (!$preliminary_completion) ? '' : 'pri-disabled';
        $cursor = (!$preliminary_completion) ? '' : 'pri-cursor';

        $data = [
            'preliminary_progress' => $preliminary_completion,
            'assessment_completion' => $assessment_completion,
            'constraints' => $constraints,
            'chapters' => $chapters,
            'disabled' => $disabled,
            'cursor' => $cursor
        ];

        return $data;
    }

    /**
     * Check the progress of the prelimary check
     *
     * @return int
     */
    public static function getPreliminaryCompletion()
    {
        return DB::table('assessments')->where('user_id', Auth::id())->value('preliminary_check');
    }

    /**
     * Return the constraints with their associated values for the preliminary check.
     *
     * @param \App\Assessment $assessment_id
     * @return \App\Constraint $constraints
     */
    public static function getPreliminaryConstraints()
    {
        $constraints = Constraint::where('title', '!=', 'Test')->get();
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        foreach ($constraints as $constraint) {
            $constraint_entry = DB::table('assessment_constraints')->where([
                'assessment_id' => $assessment_id,
                'constraint_id' => $constraint->id
            ])->first();

            $questions = explode(";", $constraint->question);

            if (count($questions) > 1) {
                for ($i = 0; $i < count($questions) - 1; $i++) {
                    $constraint->multiple_questions[] = $questions[$i];
                }
            }

            /**
             * Check if the constraint has already been answered.
             * If not, default null values will be displayed in the
             * preliminary check. If a constraints has multiple questions,
             * they have to be prepared as well as their given answers.
             *
             *
             */
            if ($constraint_entry) {
                if (strpos($constraint_entry->answer, ';') !== false) {
                    $multi_answers = explode(';', $constraint_entry->answer);

                    for ($i = 0; $i < count($multi_answers) - 1; $i++) {
                        $constraint->multiple_answers[] = $multi_answers[$i];

                        if ($multi_answers[$i] == '1') {
                            $constraint->multiple_checked_yes[$i] = 'checked';
                            $constraint->multiple_checked_no[$i] = '';
                        } else {
                            $constraint->multiple_checked_yes[$i] = '';
                            $constraint->multiple_checked_no[$i] = 'checked';
                        }
                    }
                } else {
                    $constraint->answer = $constraint_entry->answer;

                    if ($constraint->answer == '1') {
                        $constraint->checked_yes = 'checked';
                    } elseif ($constraint->answer === '0') {
                        $constraint->checked_no = 'checked';
                    }
                }
            } else {
                for ($i = 0; $i < count($questions) - 1; $i++) {
                    $constraint->multiple_checked_yes[$i] = '';
                    $constraint->multiple_checked_no[$i] = '';
                }
            }
        }

        return $constraints;
    }

    /**
     * Read the amount of applicable constraints based
     * on the answers from the preliminary check
     *
     * @param $preliminary_answers
     * @return int
     */
    public function readPreliminaryCount($preliminary_answers)
    {
        $preliminary_count = 0;

        foreach ($preliminary_answers as $preliminary_answer) {
            if ($preliminary_answer == 1) {
                $preliminary_count++;
            }
        }

        return $preliminary_count;
    }

    /**
     * Save the answers from the preliminary check on user input.
     * This function will be called on every change to provide autosaving functionality.
     *
     *
     */
    public function updatePreliminaryCheck($id, $answer)
    {
        $results = DB::table('assessment_constraints')
            ->join('assessments', 'assessments.id', '=', 'assessment_constraints.assessment_id')
            ->where([
                'assessment_constraints.constraint_id' => $id,
                'user_id' => Auth::id()
            ])
            ->select('assessment_constraints.id')
            ->get();

        if (count($results) != 0) {
            DB::table('assessment_constraints')
                ->where('constraint_id', $id)
                ->update(['answer' => $answer]);
        } else {
            $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

            DB::table('assessment_constraints')->insert(
                [
                    'answer' => $answer,
                    'comment' => null,
                    'filename' => null,
                    'mime' => null,
                    'original_filename' => null,
                    'constraint_id' => $id,
                    'assessment_id' => $assessment_id,
                    'created_at' => Carbon::now()
                ]
            );
        }

        return 1;
    }

    /**
     * Submit the preliminary check and update the progress of the assessment
     *
     *
     */
    public function submitPreliminaryCheck()
    {
        return DB::table('assessments')->where('user_id', Auth::id())->update(['preliminary_check' => 1]);
    }

    /**
     * Reset the assessment and remove all the answers for the preliminary
     * check as well as the answers for duties and features
     *
     *
     */
    public function resetPreliminaryCheck()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        DB::table('assessments')->where('id', $assessment_id)->update(['preliminary_check' => 0], ['report_ready' => 0]);

        DB::table('assessment_chapters')->where('assessment_id', $assessment_id)->delete();
        DB::table('assessment_constraints')->where('assessment_id', $assessment_id)->delete();
        DB::table('assessment_dispensations')->where('assessment_id', $assessment_id)->delete();
        DB::table('assessment_duties')->where('assessment_id', $assessment_id)->delete();
        DB::table('assessment_features')->where('assessment_id', $assessment_id)->delete();

        #Session::flush();

        return 1;
    }

    /**
     * Check if all prelimary answers are given for possible submission
     *
     *
     */
    public function preliminaryComplete()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        $constraint_count = Constraint::count();
        $assessment_constraint_count = DB::table('assessment_constraints')->select('constraint_id')->where('assessment_id', $assessment_id)->get()->count();

        if ($constraint_count != $assessment_constraint_count) {
            return 0;
        }

        return 1;
    }

    /**
     * Prepare the chapters of the dashboard and check if a chapter contains applicable
     * duties to show it to the user. If no applicable duties are present, then the chapter
     * will be omitted in the dashboard and the side navigation.
     *
     * @return \App\Chapter $chapters
     */
    public static function getChapterOverview()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        $chapters = Chapter::all();

        $duty_statement =
            'SELECT duties.title AS duty_title,
                    duties.constraint_id,
                    constraints.title AS constraint_title,
                    assessment_constraints.answer,
                    paragraphs.number AS paragraph_number,
                    articles.title AS article_title,
                    chapters.title AS chapter_title,
                    chapters.chapter_order
                FROM duties
                    LEFT JOIN constraints
                        ON constraints.id = duties.constraint_id
                    LEFT JOIN assessment_constraints
                        ON assessment_constraints.constraint_id = constraints.id
                    LEFT JOIN assessments
                 	    ON assessments.id = assessment_constraints.assessment_id
                    LEFT JOIN users
                 	    ON users.id = assessments.id
                	LEFT JOIN paragraphs
                    	ON paragraphs.id = duties.paragraph_id
                    LEFT JOIN articles
                    	ON articles.id = paragraphs.article_id
                    LEFT JOIN chapters
                    	ON chapters.id = articles.chapter_id
                WHERE assessment_id = ? AND assessment_constraints.answer NOT LIKE ?
                      OR constraints.id = 9
                ORDER BY chapter_order
                ';

        $duties = DB::select($duty_statement, [$assessment_id, "%0%"]);

        $i = 0;

        foreach ($chapters as $chapter) {
            $chapter->duty_count = 0;

            foreach ($duties as $duty) {
                if ($duty->chapter_title == $chapter->title) {
                    $chapter->duty_count++;
                }
            }

            $chapter->cursor = ($chapter->duty_count != 0) ? '' : 'pri-cursor';
            $chapter->disabled = ($chapter->duty_count != 0) ? '' : 'pri-disabled';

            $assessment_chapter = Chapter::getProgress($chapter->id);
            $chapter->status = $assessment_chapter['status'];
            $chapter->submission = $assessment_chapter['submission'];

            if ($chapter->duty_count == 0) {
                unset($chapters[$i]);
            }

            $i++; 
        }

        return $chapters;
    }

    /**
     * Set the status of the general completion based on the applicable chapters
     * 
     *
     * @return int
     */
    public static function setAssessmentCompletion($chapters)
    {
        $report_ready = 1;
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $chapter_ids = [];

        foreach ($chapters as $chapter) {
            $chapter_ids[] = $chapter->id;
        }        

        $assessment_chapters = DB::table('assessment_chapters')->whereIn('chapter_id', $chapter_ids)->where('assessment_id', $assessment_id)->get();

        /* check if all chapters are available in the table and if they are completed with 100 % */
        if (count($assessment_chapters) != count($chapters)) {
            $report_ready = 0;
        }

        foreach ($assessment_chapters as $assessment_chapter) {
            if ($assessment_chapter->status != 100.00) {
                $report_ready = 0;
            }
        }

        DB::table('assessments')->where('user_id', Auth::id())->update(['report_ready' => $report_ready]);
    }

    /**
     * Retrieve the status of the general completion for the further
     * creation of the reports and completion of the GDPR Assessment.
     *
     * @return int
     */
    public static function getAssessmentCompletion()
    {
        return DB::table('assessments')->select('report_ready')->where('user_id', Auth::id())->value('report_ready');
    }
}
