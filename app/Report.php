<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Setasign\Fpdf\FPDF;
use Mpdf\Mpdf\src\Mpdf as Mpdf;
use Auth;

class Report extends Model
{
    public $data;
    public $pdf;

    public $results;
    public $fulfilled;
    public $gaps;
    public $critical;

    public function __construct($data)
    {
        $this->data = $data;
        $this->pdf = new \Mpdf\Mpdf;
    }

    public function collectData()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        foreach ($this->data['chapters'] as $chapter) {
            if ($chapter->disabled != 'pri-disabled') {
                $chapter_progress = Chapter::getProgress($chapter->id);

                /* $chapter_progress == 1 */
                if (1) {
                    $chapter->fulfilled = [];
                    $chapter->gaps = [];
                    $chapter->critical = [];

                    $duties_statement =
                        'SELECT duties.id AS id,
                                duties.title AS duty_title,
                                duties.information AS duty_information,
                                duties.question AS duty_question,
                                duties.constraint_id,
                                constraints.title AS constraint_title,
                                assessment_constraints.answer,
                                paragraphs.number AS paragraph_number,
                                articles.title AS article_title,
                                chapters.title AS chapter_title,
                                chapters.chapter_order
                            FROM duties
                                LEFT JOIN constraints
                                    ON constraints.id = duties.constraint_id
                                LEFT JOIN assessment_constraints
                                    ON assessment_constraints.constraint_id = constraints.id
                                LEFT JOIN assessments
                                    ON assessments.id = assessment_constraints.assessment_id
                                LEFT JOIN users
                                    ON users.id = assessments.id
                                LEFT JOIN paragraphs
                                    ON paragraphs.id = duties.paragraph_id
                                LEFT JOIN articles
                                    ON articles.id = paragraphs.article_id
                                LEFT JOIN chapters
                                    ON chapters.id = articles.chapter_id
                            WHERE assessment_id = ? AND assessment_constraints.answer NOT LIKE ?
                                  OR constraints.id = 17
                            HAVING chapter_title = ?
                            ';

                    $duties = DB::select($duties_statement, [$assessment_id, "%0%", $chapter->title]);

                    foreach ($duties as $duty) {
                        $assessment_duty = Duty::getAnswer($duty->id);

                        $duty->answer = $assessment_duty->answer;
                        $duty->comment = $assessment_duty->comment;
                        $duty->attachment = $assessment_duty->original_filename;

                        $duty->flag = $duty->answer;


                        /** Collect the dispensations associated with the duty
                         *
                         */
                        $duty->dispensations = [];

                        $dispensations_statement =
                            'SELECT dispensations.id AS id,
                                    dispensations.title AS dispensation_title,
                                    dispensations.information AS dispensation_information,
                                    dispensations.question AS dispensation_question,
                                    paragraphs.number AS paragraph_number
                                 FROM dispensations
                                    JOIN duty_dispensations
                                        ON duty_dispensations.dispensation_id = dispensations.id
                                    JOIN duties
                                        ON duties.id = duty_dispensations.duty_id
                                    JOIN paragraphs
                                        ON paragraphs.id = dispensations.paragraph_id
                                 WHERE duties.id = ?
                                 ORDER BY duty_id
                                ';

                        $dispensations = DB::select($dispensations_statement, [$duty->id]);

                        foreach ($dispensations as $dispensation) {
                            $assessment_dispensation = Dispensation::getAnswer($dispensation->id, $duty->id);

                            $dispensation->answer = $assessment_dispensation->answer;
                            $dispensation->comment = $assessment_dispensation->comment;
                            $dispensation->attachment = $assessment_dispensation->original_filename;
                            $dispensation->duty_id = $assessment_dispensation->duty_id;

                            $duty->dispensations[] = $dispensation;

                            if ($dispensation->answer == 0) {
                                $duty->flag = 3;
                            }


                            /** Collect the dispensation features associated with the duty
                             *
                             */
                            $dispensation->features = [];

                            $dispensation_features_statement =
                                'SELECT features.id AS id,
                                        features.title AS dispensation_feature_title,
                                        features.information AS dispensation_feature_information,
                                        features.question AS dispensation_feature_question,
                                        paragraphs.number AS paragraph_number,
                                        assessment_constraints.answer
                                    FROM features
                                        LEFT JOIN constraints
                                            ON constraints.id = features.constraint_id
                                        LEFT JOIN assessment_constraints
                                            ON assessment_constraints.constraint_id = constraints.id
                                        LEFT OUTER JOIN assessments
                                            ON assessments.id = assessment_constraints.assessment_id
                                        LEFT OUTER JOIN users
                                            ON users.id = assessments.id
                                        JOIN paragraphs
                                            ON paragraphs.id = features.paragraph_id
                                        JOIN dispensation_features
                                            ON dispensation_features.feature_id = features.id
                                        JOIN dispensations
                                            ON dispensations.id = dispensation_features.dispensation_id
                                        WHERE dispensations.id = ?
                                            AND assessment_id = ?
                                            AND assessment_constraints.answer NOT LIKE ?
                                            OR (dispensations.id = ? AND features.constraint_id IS NULL)
                                        ORDER BY dispensations.id
                                    ';

                            $dispensation_features = DB::select($dispensation_features_statement, [3, $assessment_id, "%0%", 3]);

                            foreach ($dispensation_features as $dispensation_feature) {
                                $assessment_dispensation_feature = Feature::getAnswer($dispensation_feature->id);

                                $dispensation_feature->answer = $assessment_dispensation_feature->answer;
                                $dispensation_feature->comment = $assessment_dispensation_feature->comment;
                                $dispensation_feature->attachment = $assessment_dispensation_feature->original_filename;

                                $dispensation->features[] = $dispensation_feature;

                                if ($dispensation_feature->answer == 0) {
                                    $duty->flag = 3;
                                }
                            }
                        }


                        /** Collect the features associated with the duty
                         *
                         */
                        $duty->features = [];

                        $features_statement =
                            'SELECT features.id AS id,
                                    features.title AS feature_title,
                                    features.information AS feature_information,
                                    features.question AS feature_question,
                                    paragraphs.number AS paragraph_number,
                                    assessment_constraints.answer
                                FROM features
                                    LEFT JOIN constraints
                                        ON constraints.id = features.constraint_id
                                    LEFT JOIN assessment_constraints
                                        ON assessment_constraints.constraint_id = constraints.id
                                    LEFT OUTER JOIN assessments
                                        ON assessments.id = assessment_constraints.assessment_id
                                    LEFT OUTER JOIN users
                                        ON users.id = assessments.id
                                    JOIN paragraphs
                                        ON paragraphs.id = features.paragraph_id
                                    JOIN duty_features
                                        ON duty_features.feature_id = features.id
                                    JOIN duties
                                        ON duties.id = duty_features.duty_id
                                WHERE duties.id = ?
                                      AND assessment_id = ?
                                      AND assessment_constraints.answer NOT LIKE ?
                                      OR (duties.id = ? AND features.constraint_id IS NULL)
                                ORDER BY duty_id
                            ';

                        $features = DB::select($features_statement, [$duty->id, $assessment_id, "%0%", $duty->id]);

                        foreach ($features as $feature) {
                            $assessment_feature = Feature::getAnswer($feature->id);

                            $feature->answer = $assessment_feature->answer;
                            $feature->comment = $assessment_feature->comment;
                            $feature->attachment = $assessment_feature->original_filename;
                            $feature->duty_id = $assessment_feature->duty_id;

                            $duty->features[] = $feature;

                            if ($feature->answer == 0) {
                                $duty->flag = 0;
                            }


                            /** Collect the related features associated with the duty
                             *
                             */
                            $feature->related_features = [];

                            $related_features_statement =
                                 'SELECT b.id,
                                         b.title AS related_feature_title,
                                         b.information AS related_feature_information,
                                         b.question AS related_feature_question,
                                         paragraphs.number AS paragraph_number,
                                         assessment_constraints.answer
                                     FROM features
                                         LEFT JOIN constraints
                                             ON constraints.id = features.constraint_id
                                         LEFT JOIN assessment_constraints
                                             ON assessment_constraints.constraint_id = constraints.id
                                         LEFT OUTER JOIN assessments
                                             ON assessments.id = assessment_constraints.assessment_id
                                         LEFT OUTER JOIN users
                                             ON users.id = assessments.id
                                         JOIN paragraphs
                                             ON paragraphs.id = features.paragraph_id
                                         JOIN feature_features
                                             ON feature_features.feature_id = features.id
                                         JOIN features as b
                                             ON b.id = feature_features.feature_related_id
                                         WHERE features.id = ?
                                             AND assessment_id = ?
                                             AND assessment_constraints.answer NOT LIKE ?
                                             OR (features.id = ? AND features.constraint_id IS NULL)
                                 ';

                            $related_features = DB::select($related_features_statement, [$feature->id, $assessment_id, "%0%", $feature->id]);

                            foreach ($related_features as $related_feature) {
                                $assessment_related_feature = Feature::getAnswer($related_feature->id);

                                $related_feature->answer = $assessment_related_feature->answer;
                                $related_feature->comment = $assessment_related_feature->comment;
                                $related_feature->attachment = $assessment_related_feature->original_filename;

                                $feature->related_features[] = $related_feature;

                                if ($related_feature->answer == 0) {
                                    $duty->flag = 0;
                                }
                            }
                        }


                        /** Determine in which part of the evaluation the duty and all relevant
                         *  features and dispensations will be placed.
                         */
                        switch ($duty->flag) {
                            case 0:
                            case 2:
                                $chapter->critical = array_merge($chapter->critical, [$duty]);
                                break;
                            case 1:
                                $chapter->fulfilled = array_merge($chapter->fulfilled, [$duty]);
                                break;
                            case 3:
                                $chapter->gaps = array_merge($chapter->gaps, [$duty]);
                                break;
                        }
                    }
                }
            }
        }
    }



    public function generateStats()
    {
        foreach ($this->data['chapters'] as $chapter) {
            $chapter->positive_duty_count = 0;
            $chapter->negative_duty_count = 0;
            $chapter->not_applicable_duty_count = 0;

            $chapter->positive_feature_count = 0;
            $chapter->negative_feature_count = 0;
            $chapter->not_applicable_feature_count = 0;

            if (count($chapter->fulfilled) != 0) {
                foreach ($chapter->fulfilled as $fulfilled) {
                    $chapter->positive_duty_count++;

                    foreach ($fulfilled->features as $feature) {
                        $chapter->positive_feature_count++;
                    }
                }
            }

            if (count($chapter->critical) != 0) {
                foreach ($chapter->critical as $critical) {
                    $chapter->negative_duty_count++;

                    foreach ($critical->features as $feature) {
                        $chapter->negative_feature_count++;

                        $feature->color = ($feature->answer == 0) ? 'red' : 'green';
                    }
                }
            }


            #$featureCount = 0;
            $chapterUnsureDutyCount = 0;
            $applicableDutyCount = $chapter->positive_duty_count + $chapter->negative_duty_count + $chapterUnsureDutyCount;

            $featureCount = $chapter->positive_feature_count + $chapter->negative_feature_count;

            /*********************
            ***division by zero for unanswered chapters, fix in final version!***
            *********************/
            if ($applicableDutyCount == 0) {
                $fulfilledDutyStatus = 1;
                $applicableDutyStatus = 1;
                $fulfilledFeatureStatus = 1;
                $applicableFeatureStatus = 1;

                $fulfilledTotalStatus = 1;
            } else {
                $fulfilledDutyStatus = $chapter->positive_duty_count / $applicableDutyCount * 100;
                $fulfilledFeatureStatus = $chapter->positive_feature_count / $featureCount * 100;
            }

            $applicableTotalCount = $applicableDutyCount + $featureCount;

            if ($applicableDutyCount != 0) {
                $fulfilledTotalStatus = ($chapter->positive_duty_count + $chapter->positive_feature_count) / $applicableTotalCount * 100;
            } else {
                $fulfilledTotalStatus = 1;
            }

            $fulfilledTotalStatus = 1;
        }
    }


    public function createReport()
    {
        $file_name = 'report_franjo_antunovic_1';

        /*$this->pdf->AddPage();
        $this->pdf->SetFont('Arial', 'B', 16);
        $this->pdf->Cell(40,10,'');
        $this->pdf->Output('F', APP_PATH . 'documents/' . $fileName . '.pdf');*/

        $mpdf = new \Mpdf\Mpdf([
            'margin_top' => 0,
            'margin_left' => 30,
            'margin_right' => 10,
            'mirrorMargins' => true
        ]);

        $mpdf->setFooter('{PAGENO}');

        #$mpdf->SetProtection(array('copy','print'), '', 'MyPassword');
        $stylesheet = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/css/print.css');

        $mpdf->AddPage();
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($this->data);
        $mpdf->Output($_SERVER['DOCUMENT_ROOT'] . '/../storage/app/reports/' . $file_name . '.pdf', \Mpdf\Output\Destination::FILE);

        return 1;
    }
}
