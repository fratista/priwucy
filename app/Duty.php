<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Response;
use Session;
use Carbon\Carbon;

class Duty extends Model
{
    protected $table = 'duties';

    public $store;

    public function __construct($duty = null, $paragraph = null, array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
     * Save the stored user anwer for a specific duty
     *
     * @param \Request $request
     * @return int
     */
    public function saveAnswer($request)
    {
        $attachment = [
            'filename' => 'test',
            'mime' => 'jpeg2wbmp',
            'original_filename' => ''
        ];

        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_duty = DB::table('assessment_duties')
            ->where([
                'duty_id' => $request->duty_id,
                'assessment_id' => $assessment_id
            ])->first();

        if ($assessment_duty) {
            DB::table('assessment_duties')
                ->where([
                    'duty_id' => $request->duty_id,
                    'assessment_id' => $assessment_id
                ])
                ->update([
                    'answer' => $request->answer,
                    'comment' => $request->comment,
                    'filename' => $attachment['filename'],
                    'mime' => $attachment['mime'],
                    'original_filename' => $attachment['original_filename'],
                    'updated_at' => Carbon::now()
                ]);

            return 2;
        } else {
            DB::table('assessment_duties')->insert([
                'answer' => $request->answer,
                'comment' => $request->comment,
                'filename' => $attachment['filename'],
                'mime' => $attachment['mime'],
                'original_filename' => $attachment['original_filename'],
                'duty_id' => $request->duty_id,
                'assessment_id' => $assessment_id,
                'created_at' => Carbon::now()
            ]);

            return 1;
        }

        return 0;
    }

    /**
     * Return the stored user anwer for a specific duty
     *
     * @param $duty_id
     * @return $assessment_duty
     */
    public static function getAnswer($duty_id)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_duty = DB::table('assessment_duties')
            ->where([
                'duty_id' => $duty_id,
                'assessment_id' => $assessment_id
            ])->first();

        if (!$assessment_duty) {
            $assessment_duty = collect();
            $assessment_duty->answer = 3;
            $assessment_duty->comment = '';
            $assessment_duty->original_filename = 'No file selected';
        }

        return $assessment_duty;
    }
}
