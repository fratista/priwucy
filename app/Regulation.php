<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Chapter;
use App\Constraint;

class Regulation extends Model
{
    public $duties = [];
    public $distinctDuties;

    public $disabled;
    public $cursor;

    private $store;

    public $html;

    public function __construct()
    {
        $this->store = Store::getInstance();
    }

    /**
     * Retrieve all available triples from the RDF model and display
     * them in a table showing the subject, predicate and object
     *
     *
     */
    public function retrieveAll()
    {
        $q = 'SELECT DISTINCT ?subject ?property ?object WHERE { ?subject ?property ?object . }';

        $rows = $this->store->sparq($q, 'rows');
        $r = '';
        if ($rows = $this->store->sparq($q, 'rows')) {
            $r = '<table class="container table table-striped table-hover">
            <thead class="container thead-inverse">
            <th class="col-md-4">Subject</th><th class="col-md-4">Property</th><th class="col-md-4">Object</th>'."\n";

            foreach ($rows as $row) {
                $r .= '<tr><td class="col-md-4">'.$row['subject'] .
                '</td><td class="col-md-4">'.$row['property'] .
                '</td><td class="col-md-4">'.$row['object'] . '</td></tr>'."\n";
            }

            $r .='</table>'."\n";
        }

        return $r;
    }

    /**
     * Retrieve the preliminary constraints from the RDF model
     * for use in the preliminary check to further filter the
     * applicable duties and features for the assessment.
     *
     */
    public function retrieveConstraints()
    {
        $q = '
        SELECT ?constraint ?title ?information ?question ?constraint_order (COUNT(?question) AS ?question_count)
        WHERE {
            ?constraint syntax:type odrl:Constraint ;
            gdpr:titletool ?title ;
            gdpr:relatedtext ?information ;
            gdpr:questiontool ?question ;
            rdfs:comment ?constraint_order                
        } GROUP BY ?constraint
        ORDER BY ?constraint';

        $constraints = $this->store->sparq($q);

        foreach ($constraints as $const) {
            $constraint = new Constraint;

            $constraint->uri = $const['constraint'];
            $constraint->title = $const['title'];
            $constraint->information = $const['information'];
            $constraint->question = $this->retrieveQuestions(Helper::wrapEntity($const['constraint']));
            $constraint->constraint_order = $const['constraint_order'];

            $constraint->save();
        }

        session(['constraints' => true]);
    }

    public function retrieveQuestions($constraint)
    {
        $q = '
        SELECT ?question
        WHERE {
            ' . $constraint . ' gdpr:questiontool ?question

        }';

        $questions = $this->store->sparq($q);

        /**
         * Handle multiple questions per constraint and display them as a group.
         *
         *
         */
        if (count($questions) > 1) {
            $multipleQuestions = '';

            foreach ($questions as $question) {
                $multipleQuestions .= $question['question'] . ';';
            }

            return $multipleQuestions;
        } else {
            foreach ($questions as $question) {
                return $question['question'];
            }
        }
    }

    /**
     * Retrieve the available chapters from the RDF store
     *
     *
     *
     */
    public function retrieveChapters()
    {
        $q = '
        SELECT ?chapter ?title ?chapter_order 
        WHERE {
            ?chapter syntax:type gdpr:Chapter ;
            gdpr:titletool ?title ;
            rdfs:comment ?chapter_order
        }';
        
        $chapters = $this->store->sparq($q);

        foreach ($chapters as $chapt) {
            $chapter = new Chapter;

            $chapter->uri = $chapt['chapter'];
            $chapter->title = $chapt['title'];
            $chapter->chapter_order = $chapt['chapter_order'];

            $chapter->save();
        }

        session(['chapters' => true]);
    }

    /**
     * Retrieve the order of the chapters for the placement
     * in the GDPR Compliance Dashboard.
     *
     *
     */
    public function chapterOrder()
    {
        $result = DB::table('chapters')->select('title', 'chapter_order')->orderBy('chapter_order')->get();

        $chapterOrder = [];

        foreach ($result as $r) {
            $chapterOrder[$r->title] = $r->chapter_order;
        }

        return $chapterOrder;
    }

    /**
     * Retrieve the articles from the regulation.
     *
     *
     */
    public function retrieveArticles($chapters)
    {
        foreach ($chapters as $chapter) {
            $uri = Helper::wrapEntity($chapter->uri);

            $articles = $this->store->sparq(
                '
                SELECT ?article
                WHERE {
                    ?article syntax:type gdpr:Article ;
                                gdpr:definedin ' . $uri . '
                }'
            );

            /**
             * Extract the values for the database based on the original URI.
             *
             *
             */
            foreach ($articles as $art) {
                $parts = explode('/', $art['article']);
                $subparts = explode('.', $parts[4]);

                $articleNumber = $subparts[0];
                $articleTitle = preg_replace('/([a-z0-9])?([A-Z])/', '$1 $2', $subparts[1]);

                $article = new Article;

                $article->uri = $art['article'];
                $article->title = $articleTitle;
                $article->number = $articleNumber;
                $article->chapter_id = $chapter->id;

                $article->save();
            }
        }

        session(['articles' => true]);
    }

    /**
     * Retrieve the paragraphs from the regulation.
     *
     *
     */
    public function retrieveParagraphs()
    {
        $paragraphs = $this->store->sparq(
            '
            SELECT ?paragraph ?information ?definedin
            WHERE {
                ?paragraph syntax:type gdpr:Paragraph ;
                           gdpr:relatedtext ?information ;
                           gdpr:definedin ?definedin
            }'
        );

        foreach ($paragraphs as $par) {
            $parts = explode('/', $par['paragraph']);
            $paragraphNumber = Helper::extractName($par['paragraph']);

            $paragraph = new Paragraph;

            $paragraph->uri = $par['paragraph'];
            $paragraph->number = $paragraphNumber;
            $paragraph->information = $par['information'];

            $paragraph->article_id = Article::where('uri', $par['definedin'])->first()->id;

            $paragraph->save();
        }

        session(['paragraphs' => true]);
    }

    /**
     * Retrieve the duties from the regulation.
     *
     *
     */
    public function retrieveDuties()
    {
        $duties = $this->store->sparq(
            '
            SELECT DISTINCT ?duty ?title ?information ?question ?definedin ?constraint
            WHERE {
                ?duty syntax:type odrl:Duty ;
                      gdpr:titletool ?title ;
                      gdpr:relatedtext ?information ;
                      gdpr:questiontool ?question ;
                      gdpr:definedin ?definedin ;
                      gdpr:constraint ?constraint
            }
            GROUP BY ?duty
            ORDER BY ?duty'
        );

        foreach ($duties as $dut) {
            $duty = new Duty;

            $duty->uri = $dut['duty'];
            $duty->title = $dut['title'];
            $duty->information = $dut['information'];
            $duty->question = $dut['question'];

            $paragraph = Paragraph::where('uri', $dut['definedin'])->first();

            if ($paragraph) {
                $duty->paragraph_id = $paragraph->id;
            }

            $duty->constraint_id = Constraint::where('uri', $dut['constraint'])->first()->id;

            $duty->save();
        }

        session(['duties' => true]);
    }

    /**
     * Retrieve the dispensations from the regulation.
     *
     *
     */
    public function retrieveDispensations()
    {
        $dispensations = $this->store->sparq(
            '
            SELECT DISTINCT ?dispensation ?title ?question ?information ?definedin
            WHERE {
                ?dispensation syntax:type gdpr:Dispensation ;
                      gdpr:titletool ?title ;
                      gdpr:questiontool ?question ;
                      gdpr:relatedtext ?information .
                      OPTIONAL { ?dispensation gdpr:definedin ?definedin }
            }
            GROUP BY ?dispensation
            ORDER BY ?dispensation'
        );

        foreach ($dispensations as $disp) {
            $dispensation = new Dispensation;

            $dispensation ->uri = $disp['dispensation'];
            $dispensation ->title = $disp['title'];
            $dispensation ->information = $disp['information'];
            $dispensation ->question = $disp['question'];

            try {
                $paragraph = Paragraph::where('uri', $disp['definedin'])->first();

                if ($paragraph) {
                    $dispensation->paragraph_id = $dispensation->id;
                }
            } catch (Exception $ex) {
                $dispensation->paragraph_id = null;
            }

            $dispensation->save();
        }

        session(['dispensations' => true]);
    }

    /**
     * Retrieve the features from the regulation.
     *
     *
     */
    public function retrieveFeatures()
    {
        $features = $this->store->sparq(
            '
            SELECT DISTINCT ?feature ?title ?information ?question ?definedin ?constraint
            WHERE {
                ?feature syntax:type gdpr:Feature ;
                      gdpr:titletool ?title ;
                      gdpr:relatedtext ?information ;
                      gdpr:questiontool ?question ;
                      gdpr:definedin ?definedin .
                      FILTER regex(str(?definedin), "([0-9])+", "i") .
                      OPTIONAL { ?feature gdpr:constraint ?constraint }
            }
            GROUP BY ?feature
            ORDER BY ?feature'
        );

        foreach ($features as $feat) {
            $feature = new Feature;

            $feature->uri = $feat['feature'];
            $feature->title = $feat['title'];
            $feature->information = $feat['information'];
            $feature->question = $feat['question'];

            $paragraph_id = Paragraph::where('uri', $feat['definedin'])->first()->id;

            $feature->paragraph_id = $paragraph_id;
            $feature->constraint_id = null;

            if (isset($feat['constraint'])) {
                $constraint_id = Constraint::where('uri', $feat['constraint'])->first()->id;
                $feature->constraint_id = $constraint_id;
            }

            $feature->save();
        }

        session(['features' => true]);
    }

    /**
     * Retrieve the duty dispensations from the regulation.
     *
     *
     */
    public function retrieveDutyDispensations()
    {
        $duty_dispensations = $this->store->sparq(
            '
            SELECT DISTINCT ?duty ?dispensation
            WHERE {
                ?duty syntax:type odrl:Duty ;
                      gdpr:dispensation ?dispensation
            }'
        );

        foreach ($duty_dispensations as $duty_disp) {
            $duty_dispensation = new DutyDispensation;
            $duty_dispensation->duty_id = Duty::where('uri', $duty_disp['duty'])->first()->id;

            $dispensation = Dispensation::where('uri', $duty_disp['dispensation'])->first();

            if ($dispensation) {
                $duty_dispensation->dispensation_id = $dispensation->id;
            }

            $duty_dispensation->save();
        }
    }

    /**
     * Retrieve the duty features from the regulation.
     *
     *
     */
    public function retrieveDutyFeatures()
    {
        $duty_features = $this->store->sparq(
            '
            SELECT DISTINCT ?duty ?feature
            WHERE {
                ?duty syntax:type odrl:Duty .
                OPTIONAL { ?duty gdpr:feature ?feature }
            }'
        );

        foreach ($duty_features as $duty_feat) {
            $duty_feature = new DutyFeature;
            $duty_feature->duty_id = Duty::where('uri', $duty_feat['duty'])->first()->id;

            if (isset($duty_feat['feature'])) {
                $feature = Feature::where('uri', $duty_feat['feature'])->first();

                if ($feature) {
                    $duty_feature->feature_id = $feature->id;
                }
            } else {
                $duty_feature->feature_id = null;
            }

            $duty_feature->save();


            #echo $duty_feat['duty'].':::'.$duty_feat['feature'].'<br>';
        }
    }

    /**
     * Retrieve the dispensation features from the regulation.
     *
     *
     */
    public function retrieveDispensationFeatures()
    {
        $dispensation_features = $this->store->sparq(
            '
            SELECT ?dispensation ?feature
            WHERE {
                ?dispensation syntax:type gdpr:Dispensation ;
                              gdpr:feature ?feature
            }'
        );

        foreach ($dispensation_features as $dispensation_feat) {
            $dispensation_feature = new DispensationFeature;
            $dispensation_feature->dispensation_id = Dispensation::where('uri', $dispensation_feat['dispensation'])->first()->id;
            $feature = Feature::where('uri', $dispensation_feat['feature'])->first();

            if ($feature) {
                $dispensation_feature->feature_id = $feature->id;
            }

            $dispensation_feature->save();
        }
    }

    /**
     * Retrieve the feature features from the regulation.
     *
     *
     */
    public function retrieveFeatureFeatures()
    {
        $feature_features = $this->store->sparq(
            '
            SELECT ?feature ?feature_relation
            WHERE {
                ?feature syntax:type gdpr:Feature ;
                         gdpr:feature ?feature_relation
            }'
        );

        foreach ($feature_features as $feature_feat) {
            $feature_feature = new FeatureFeature;
            $feature_feature->feature_id = Feature::where('uri', $feature_feat['feature'])->first()->id;
            $feature_feature->feature_related_id = Feature::where('uri', $feature_feat['feature_relation'])->first()->id;

            $feature_feature->save();
        }
    }
}
