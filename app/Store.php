<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ARC2;

class Store extends Model
{
    private static $instance;
    private $config;
    public $store;

    public $prefix =
        'PREFIX gdpr: <http://privacylab.wu.ac.at/GDPR/>
         PREFIX syntax: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
         PREFIX odrl: <http://www.w3.org/ns/odrl/2/>
         PREFIX owl: <http://www.w3.org/2002/07/owl#>
         PREFIX rdf: <http://www.w3.org/2000/01/rdf-schema#>';

    /**
     * This method is used to make the Store class Singleton ready.
     * Any call of the getInstance() method will generate the
     * necessary singleton object.
     *
     *
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }

    /**
     * Read the configuration values for the setup of the ARC2 store.
     * If no store exists, one is initialized and populated with RDF data
     * provided by an OWL file which will be fetched by an HTTP request.
     * The location of the file MUST be another domain for now!
     *
     *
     */
    public function __construct()
    {
        $this->config = array(
            'db_host' => config('store.db_host'),
            'db_name' => config('store.db_name'),
            'db_user' => config('store.db_user'),
            'db_pwd' => config('store.db_pwd'),
            'store_name' => config('store.store_name')
        );

        $this->store = ARC2::getStore($this->config);

        if (!$this->store->isSetUp()) {
            $this->store->setUp();
            $this->store->query('LOAD <http://phpstack-253265-787883.cloudwaysapps.com/resources/gdpr_updated_2019_marchv3.owl>');
            #$this->store->query('LOAD <http://phpstack-253265-787883.cloudwaysapps.com/storage/datasets/gdpr.owl>');
        }
    }

    /**
     * Prepare the queries to be executed against the RDF store
     *
     *
     *
     */
    public function sparq($query)
    {
        $queryString = $this->prefix . $query;

        $result = $this->store->query($queryString);

        if (!$this->store->getErrors()) {
            return $result['result']['rows'];
        }
    }
}
