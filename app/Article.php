<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function __construct($article = null, array $attributes = array())
    {
        parent::__construct($attributes);
    }
}
