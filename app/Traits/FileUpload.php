<?php

namespace App\Traits;

use Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

trait FileUpload
{
    /**
     * This trait is intended for file uploads regarding
     * the assessment for the duties, features and dispensations.
     *
     */

    public function uploadFile()
    {
        $file = Request::file('filefield');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename().'.'.$extension, File::get($file));

        $file_information = [
            'mime' => $file->getClientMimeType(),
            'original_filename' => $file->getClientOriginalName(),
            'filename' => $file->getFilename().'.'.$extension
        ];

        return $file_information;
    }
}
