<?php

namespace App\Traits;

use Webpatser\Uuid\Uuid;

trait Uuids
{
    /**
     * This trait is intended for the usage of the
     * Uuid package by Webpatser. To use the package
     * the trait has to be implemented by a model.
     * The trait is also setup as an alias in the app config.
     *
     *
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}
