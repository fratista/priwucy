<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Chapter extends Model
{
    public function __construct($chapter = null, array $attributes = array())
    {
        parent::__construct($attributes);
    }

    /**
     * Get the current status of the chapter completion and use the value
     * for the rendering of the chapter section
     *
     * @param \App\Chapter $chapter_id
     * @return array
     */
    public static function getProgress($chapter_id)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_chapter = DB::table('assessment_chapters')->where(['chapter_id' => $chapter_id, 'assessment_id' => $assessment_id])->first();

        if ($assessment_chapter) {
            return [
                'status' => $assessment_chapter->status, 
                'submission' => $assessment_chapter->submission
            ];
        }

        return 0;
    }

    /**
     * Update the completion of the chapter
     *
     * @param \App\Chapter $chapter_id
     * @return int
     */
    public static function updateChapter($chapter_id, $status) {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        DB::table('assessment_chapters')
            ->updateOrInsert(
                ['chapter_id' => $chapter_id, 'assessment_id' => $assessment_id],
                ['status' => $status]
            );
        
        return 1;
    }

    /**
     * Submit all stored answers for the specified chapter
     *
     * @param \App\Chapter $chapter_id
     * @return int
     */
    public static function submitChapter($chapter_id)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        DB::table('assessment_chapters')
            ->updateOrInsert(
                ['chapter_id' => $chapter_id, 'assessment_id' => $assessment_id],
                ['submission' => 1]
            );
        
        return 1;      
    }

    /**
     * Reset the stored answers for the specified chapter
     *
     * @param \App\Chapter $chapter_uri
     * @return int
     */
    public static function resetChapter($chapter_id)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        $delete_features ='DELETE assessment_features FROM assessment_features
                             LEFT JOIN features
                               ON features.id = assessment_features.feature_id      
                             LEFT JOIN paragraphs
                               ON paragraphs.id = features.paragraph_id
                             LEFT JOIN articles
                               ON articles.id = paragraphs.article_id
                             LEFT JOIN chapters
                               ON chapters.id = articles.chapter_id
                           WHERE assessment_id = ? AND chapters.id = ?';

        $delete_dispensations ='DELETE assessment_dispensations FROM assessment_dispensations
                                  LEFT JOIN dispensations
                                    ON dispensations.id = assessment_dispensations.dispensation_id      
                                  LEFT JOIN paragraphs
                                    ON paragraphs.id = dispensations.paragraph_id
                                  LEFT JOIN articles
                                    ON articles.id = paragraphs.article_id
                                  LEFT JOIN chapters
                                    ON chapters.id = articles.chapter_id
                                WHERE assessment_id = ? AND chapters.id = ?';

        $delete_duties ='DELETE assessment_duties FROM assessment_duties
                           LEFT JOIN duties
                             ON duties.id = assessment_duties.duty_id
                           LEFT JOIN paragraphs
                             ON paragraphs.id = duties.paragraph_id
                           LEFT JOIN articles
                             ON articles.id = paragraphs.article_id
                           LEFT JOIN chapters
                             ON chapters.id = articles.chapter_id
                         WHERE assessment_id = ? AND chapters.id = ?';

        DB::statement($delete_features, [$assessment_id, $chapter_id]);
        DB::statement($delete_dispensations, [$assessment_id, $chapter_id]);
        DB::statement($delete_duties, [$assessment_id, $chapter_id]);
        
        DB::table('assessment_chapters')->where(['chapter_id' => $chapter_id, 'assessment_id' => $assessment_id])->delete();        

        return 1;
    }

    /**
     * Get the counts of the answered duties, features and dispensations
     * for the further processing of the report and detailed analysis
     *
     * @param \App\Chapter $chapter_uri
     * @return int
     */
    public static function getAnsweredDutyCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_duties = DB::table('assessment_duties')->where('assessment_id', $assessment_id)->count();

        return $answered_duties;
    }

    public static function getAnsweredFeatureCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_features = DB::table('assessment_features')->where('assessment_id', $assessment_id)->count();

        return $answered_features;
    }

    public static function getAnsweredDispensationCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_dispensations = DB::table('assessment_dispensations')->where('assessment_id', $assessment_id)->count();

        return $answered_dispensations;
    }


    /**
     * Render the duties, features and dispensations of each chapter
     *
     * @param \App\Chapter $chapter
     * @return array
     */
    public static function renderChapter($chapter)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        $disabled = ($chapter->status != 100.00) ? '' : 'pri-disabled';
        $cursor = ($chapter->status != 100.00) ? '' : 'pri-cursor';

        /**
         * Get all the duties related to the chapter
         */
        $duties_statement =
            'SELECT duties.id AS id,
                    duties.title AS duty_title,
                    duties.information AS duty_information,
                    duties.question AS duty_question,
                    duties.constraint_id,
                    constraints.title AS constraint_title,
                    assessment_constraints.answer,
                    paragraphs.number AS paragraph_number,
                    articles.title AS article_title,
                    chapters.title AS chapter_title,
                    chapters.chapter_order
                FROM duties
                    LEFT JOIN constraints
                        ON constraints.id = duties.constraint_id
                    LEFT JOIN assessment_constraints
                        ON assessment_constraints.constraint_id = constraints.id
                    LEFT JOIN assessments
                        ON assessments.id = assessment_constraints.assessment_id
                    LEFT JOIN users
                        ON users.id = assessments.id
                    LEFT JOIN paragraphs
                        ON paragraphs.id = duties.paragraph_id
                    LEFT JOIN articles
                        ON articles.id = paragraphs.article_id
                    LEFT JOIN chapters
                        ON chapters.id = articles.chapter_id
                WHERE assessment_id = ? AND assessment_constraints.answer NOT LIKE ?
                      OR constraints.id = 17
                HAVING chapter_title = ?
                ';

        $duties = DB::select($duties_statement, [$assessment_id, "%0%", $chapter->title]);

        /**
         * Get all the features and dispensations related to te duty and
         * store them as members of the duty class
         */
        foreach ($duties as $duty) {
            $duty->features = [];
            $duty->dispensations = [];

            $features_statement =
                'SELECT features.id AS id,
                        features.title AS feature_title,
                        features.information AS feature_information,
                        features.question AS feature_question,
                        paragraphs.number AS paragraph_number,
                        assessment_constraints.answer
                    FROM features
                        LEFT JOIN constraints
                            ON constraints.id = features.constraint_id
                        LEFT JOIN assessment_constraints
                            ON assessment_constraints.constraint_id = constraints.id
                        LEFT OUTER JOIN assessments
                            ON assessments.id = assessment_constraints.assessment_id
                        LEFT OUTER JOIN users
                            ON users.id = assessments.id
                        JOIN paragraphs
                            ON paragraphs.id = features.paragraph_id
                        JOIN duty_features
                            ON duty_features.feature_id = features.id
                        JOIN duties
                            ON duties.id = duty_features.duty_id
                    WHERE duties.id = ?
                          AND assessment_id = ?
                          AND assessment_constraints.answer NOT LIKE ?
                          OR (duties.id = ? AND features.constraint_id IS NULL)
                    ORDER BY duty_id
                ';

            $features = DB::select($features_statement, [$duty->id, $assessment_id, "%0%", $duty->id]);


            /**
             * Get all related features and store them as class member
             */
            foreach ($features as $feature) {
                $feature->related_features = [];

                $related_features_statement =
                    'SELECT b.id,
                            b.title AS related_feature_title,
                            b.information AS related_feature_information,
                            b.question AS related_feature_question,
                            paragraphs.number AS paragraph_number,
                            assessment_constraints.answer
                        FROM features
                            LEFT JOIN constraints
                                ON constraints.id = features.constraint_id
                            LEFT JOIN assessment_constraints
                                ON assessment_constraints.constraint_id = constraints.id
                            LEFT OUTER JOIN assessments
                                ON assessments.id = assessment_constraints.assessment_id
                            LEFT OUTER JOIN users
                                ON users.id = assessments.id
                            JOIN paragraphs
                                ON paragraphs.id = features.paragraph_id
                            JOIN feature_features
                                ON feature_features.feature_id = features.id
                            JOIN features as b
                                ON b.id = feature_features.feature_related_id
                            WHERE features.id = ?
                                AND assessment_id = ?
                                AND assessment_constraints.answer NOT LIKE ?
                                OR (features.id = ? AND features.constraint_id IS NULL)
                    ';

                $related_features = DB::select($related_features_statement, [$feature->id, $assessment_id, "%0%", $feature->id]);

                foreach ($related_features as $related_feature) {
                    $assessment_related_feature = Feature::getAnswer($related_feature->id);

                    $related_feature->answer = $assessment_related_feature->answer;
                    $related_feature->comment = $assessment_related_feature->comment;
                    $related_feature->attachment = $assessment_related_feature->original_filename;
                    $related_feature->parent_id = $assessment_related_feature->related_feature_id;
                    $related_feature->grandparent_id = $assessment_related_feature->related_feature_duty_id;

                    $related_feature->radio = [
                        'yes' => '',
                        'no' => '',
                        'unsure' => ''
                    ];

                    switch ($related_feature->answer) {
                        case 0:
                            $related_feature->radio['no'] = 'checked';
                            break;
                        case 1:
                            $related_feature->radio['yes']= 'checked';
                            break;
                        case 2:
                            $related_feature->radio['unsure'] = 'checked';
                            break;
                    }

                    if (($feature->id == $related_feature->parent_id && $duty->id == $related_feature->grandparent_id) || $related_feature->answer == 3) {
                        $related_feature->container_class = '';
                        $related_feature->chapter_relation = '';
                        $related_feature->cursor = '';
                        $related_feature->disabled = '';
                    } else {
                        $related_feature->container_class = 'related';
                        $related_feature->chapter_relation = 'This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.';
                        $related_feature->cursor = 'pri-cursor';
                        $related_feature->disabled = 'pri-disabled';
                    }

                    $feature->related_features[] = $related_feature;
                }


                /*
                 * Manage the feature answer
                 */
                $assessment_feature = Feature::getAnswer($feature->id);

                $feature->answer = $assessment_feature->answer;
                $feature->comment = $assessment_feature->comment;
                $feature->attachment = $assessment_feature->original_filename;
                $feature->duty_id = $assessment_feature->duty_id;

                $feature->radio = [
                    'yes' => '',
                    'no' => '',
                    'unsure' => ''
                ];

                switch ($feature->answer) {
                    case 0:
                        $feature->radio['no'] = 'checked';
                        $feature->collapse = 'collapse';
                        break;
                    case 1:
                        $feature->radio['yes']= 'checked';
                        $feature->collapse = '';
                        break;
                    case 2:
                        $feature->radio['unsure'] = 'checked';
                        $feature->collapse = 'collapse';
                        break;
                    case 3:
                        $feature->collapse = 'collapse';
                }

                if ($duty->id == $feature->duty_id || $feature->answer == 3) {
                    $feature->container_class = '';
                    $feature->chapter_relation = '';
                    $feature->cursor = '';
                    $feature->disabled = '';
                } else {
                    $feature->container_class = 'related';
                    $feature->chapter_relation = 'This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.';
                    $feature->cursor = 'pri-cursor';
                    $feature->disabled = 'pri-disabled';
                }

                $duty->features[] = $feature;
            }


            /**
             * Get all dispensations of a duty and store them as a class member
             */
            $dispensations_statement =
                'SELECT dispensations.id AS id,
                        dispensations.title AS dispensation_title,
                        dispensations.information AS dispensation_information,
                        dispensations.question AS dispensation_question,
                        paragraphs.number AS paragraph_number
                     FROM dispensations
                        JOIN duty_dispensations
                            ON duty_dispensations.dispensation_id = dispensations.id
                        JOIN duties
                            ON duties.id = duty_dispensations.duty_id
                        JOIN paragraphs
                            ON paragraphs.id = dispensations.paragraph_id
                     WHERE duties.id = ?
                     ORDER BY duty_id
                    ';

            $dispensations = DB::select($dispensations_statement, [$duty->id]);

            foreach ($dispensations as $dispensation) {
                $dispensation->features = [];

                $dispensation_features_statement =
                    'SELECT features.id AS id,
                            features.title AS dispensation_feature_title,
                            features.information AS dispensation_feature_information,
                            features.question AS dispensation_feature_question,
                            paragraphs.number AS paragraph_number,
                            assessment_constraints.answer
                        FROM features
                            LEFT JOIN constraints
                                ON constraints.id = features.constraint_id
                            LEFT JOIN assessment_constraints
                                ON assessment_constraints.constraint_id = constraints.id
                            LEFT OUTER JOIN assessments
                                ON assessments.id = assessment_constraints.assessment_id
                            LEFT OUTER JOIN users
                                ON users.id = assessments.id
                            JOIN paragraphs
                                ON paragraphs.id = features.paragraph_id
                            JOIN dispensation_features
                                ON dispensation_features.feature_id = features.id
                            JOIN dispensations
                                ON dispensations.id = dispensation_features.dispensation_id
                            WHERE dispensations.id = ?
                                AND assessment_id = ?
                                AND assessment_constraints.answer NOT LIKE ?
                                OR (dispensations.id = ? AND features.constraint_id IS NULL)
                            ORDER BY dispensations.id
                        ';

                $dispensation_features = DB::select($dispensation_features_statement, [3, $assessment_id, "%0%", 3]);

                foreach ($dispensation_features as $dispensation_feature) {
                    $assessment_dispensation_feature = Feature::getAnswer($dispensation_feature->id);

                    $dispensation_feature->answer = $assessment_dispensation_feature->answer;
                    $dispensation_feature->comment = $assessment_dispensation_feature->comment;
                    $dispensation_feature->attachment = $assessment_dispensation_feature->original_filename;
                    $dispensation_feature->parent_id = $assessment_dispensation_feature->dispensation_id;
                    $dispensation_feature->grandparent_id = $assessment_dispensation_feature->dispensation_duty_id;

                    $dispensation_feature->radio = [
                        'yes' => '',
                        'no' => '',
                        'unsure' => ''
                    ];

                    switch ($dispensation_feature->answer) {
                        case 0:
                            $dispensation_feature->radio['no'] = 'checked';
                            break;
                        case 1:
                            $dispensation_feature->radio['yes']= 'checked';
                            break;
                        case 2:
                            $dispensation_feature->radio['unsure'] = 'checked';
                            break;
                    }

                    if (($dispensation->id == $dispensation_feature->parent_id && $duty->id == $dispensation_feature->grandparent_id) || $dispensation_feature->answer == 3) {
                        $dispensation_feature->container_class = '';
                        $dispensation_feature->chapter_relation = '';
                        $dispensation_feature->cursor = '';
                        $dispensation_feature->disabled = '';
                    } else {
                        $dispensation_feature->container_class = 'related';
                        $dispensation_feature->chapter_relation = 'This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.';
                        $dispensation_feature->cursor = 'pri-cursor';
                        $dispensation_feature->disabled = 'pri-disabled';
                    }

                    $dispensation->features[] = $dispensation_feature;
                }

                $assessment_dispensation = Dispensation::getAnswer($dispensation->id, $duty->id);

                $dispensation->answer = $assessment_dispensation->answer;
                $dispensation->comment = $assessment_dispensation->comment;
                $dispensation->attachment = $assessment_dispensation->original_filename;
                $dispensation->duty_id = $assessment_dispensation->duty_id;

                $dispensation->radio = [
                    'yes' => '',
                    'no' => '',
                    'unsure' => ''
                ];

                switch ($dispensation->answer) {
                    case 0:
                        $dispensation->radio['no'] = 'checked';
                        $dispensation->collapse = 'collapse';
                        break;
                    case 1:
                        $dispensation->radio['yes']= 'checked';
                        $dispensation->collapse = '';
                        break;
                    case 2:
                        $dispensation->radio['unsure'] = 'checked';
                        $dispensation->collapse = 'collapse';
                        break;
                    case 3:
                        $dispensation->collapse = 'collapse';
                }

                if ($duty->id == $dispensation->duty_id || $dispensation->answer == 3) {
                    $dispensation->container_class = '';
                    $dispensation->chapter_relation = '';
                    $dispensation->cursor = '';
                    $dispensation->disabled = '';
                } else {
                    $dispensation->container_class = 'related';
                    $dispensation->chapter_relation = 'This question has already been answered in <a href="" class="related-chapter"></a>. To change your answer, please get back to the relevant question.';
                    $dispensation->cursor = 'pri-cursor';
                    $dispensation->disabled = 'pri-disabled';
                }

                $duty->dispensations[] = $dispensation;
            }

            $assessment_duty = Duty::getAnswer($duty->id);

            $duty->answer = $assessment_duty->answer;
            $duty->comment = $assessment_duty->comment;
            $duty->attachment = $assessment_duty->original_filename;

            $duty->radio = [
                'yes' => '',
                'no' => '',
                'unsure' => ''
            ];

            $duty->feature_collapse = 'collapse';
            $duty->dispensation_collapse = 'collapse';

            switch ($duty->answer) {
                case 0:
                    $duty->radio['no'] = 'checked';
                    $duty->dispensation_collapse = '';
                    break;
                case 1:
                    $duty->radio['yes'] = 'checked';
                    $duty->feature_collapse = '';
                    break;
                case 2:
                    $duty->radio['unsure']= 'checked';
                    $duty->feature_collapse = '';
                    break;
            }

            $duty->tooltip = substr($duty->duty_information, 0, 360) . ' ...';
        }

        return $duties;
    }
}
