<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Duty;

class DutyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Store the answer of the duty.
     *
     * @param  \Request  $request
     * @param  \Duty  $duty
     * @return \Illuminate\Http\Response
     */
    public function saveAnswer(Request $request, Duty $duty)
    {
        $response = $duty->saveAnswer($request);

        return Response::json($response);
    }
}
