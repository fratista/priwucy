<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Feature;

class FeatureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Store the answer of the feature.
     *
     * @param  \Request  $request
     * @param  \Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function saveAnswer(Request $request, Feature $feature)
    {
        $response = $feature->saveAnswer($request);

        return Response::json($response);
    }

    /**
     * Delete the associated dependencies of answers when set to no.
     *
     * @param  \Request  $request
     * @param  \Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function deleteAnswers(Request $request, Feature $feature)
    {
        $response = $feature->deleteAnswers($request);

        return Response::json($response);
    }
}
