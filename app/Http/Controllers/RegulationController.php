<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App;
use App\Assessment;
use App\Regulation;
use App\Constraint;
use App\Chapter;
use App\Article;
use App\Paragraph;
use App\Duty;
use App\Feature;
use App\Dispensation;
use App\Helper;

class RegulationController extends Controller
{
    public function __construct()
    {

    }

    /**
     * In development mode, the regulation from the provided
     * data in the triple store will be queried thus leading
     * to the creation and alteration of the database tables.
     */
    public function index() {
        if (App::environment('local')) {
            $this->retrieveRegulation();
        }
    }

    /**
     * This method is used to provide the integration to initialize
     * the regulation provided by the RDF store and map it to a relational
     * database schema. Changes are only allowed to be made manually
     * while being in developer mode and if there is no current mapping
     * of the regulation in the relational database.
     *
     *
     *
     */
    public function retrieveRegulation()
    {
        try {
            Regulation::findOrFail(1);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $regulation = new Regulation();

            /**
             * Save the constraints to the database if there are non available.
             *
             */
            $constraints = Constraint::find(1);

            if (!$constraints) {
                $regulation->retrieveConstraints();
            }

            /**
             * Save the chapters to the database if there are non available.
             *
             */
            $chapters = Chapter::find(1);

            if (!$chapters) {
                $regulation->retrieveChapters();
            }

            /**
             * Save the articles to the database if there are non available.
             *W
             */
            $articles = Article::find(1);

            if (!$articles) {
                $chapters = Chapter::all();

                $regulation->retrieveArticles($chapters);
            }

            /**
             * Save the paragraphs to the database if there are non available.
             *
             */
            $paragraphs = Paragraph::find(1);

            if (!$paragraphs) {
                $regulation->retrieveParagraphs();
            }

            /**
             * Save the duties to the database if there are non available.
             *
             */
            $duties = Duty::find(1);

            if (!$duties) {
                $regulation->retrieveDuties();
            }

            /**
             * Save the dispensations to the database if there are non available.
             *
             */
            $dispensations = Dispensation::find(1);

            if (!$dispensations) {
                $regulation->retrieveDispensations();
            }

            /**
             * Save the features to the database if there are non available.
             *
             */
            $features = Feature::find(1);

            if (!$features) {
                $regulation->retrieveFeatures();
            }

            /**
             * Save the duty and dispensation relations to the database if there are non available.
             *
             */
            $duty_dispensations = DB::table('duty_dispensations')->find(1);

            if (!$duty_dispensations) {
                $regulation->retrieveDutyDispensations();
            }

            /**
             * Save the duty and feature relations to the database if there are non available.
             *
             */
            $duty_features = DB::table('duty_features')->find(1);

            if (!$duty_features) {
                $regulation->retrieveDutyFeatures();
            }

            /**
             * Save the dispensation and feature relations to the database if there are non available.
             *
             */
            $dispensation_features = DB::table('dispensation_features')->find(1);

            if (!$dispensation_features) {
                $regulation->retrieveDispensationFeatures();
            }

            /**
             * Save the duty and dispensation relations to the database if there are non available.
             *
             */
            $feature_features = DB::table('feature_features')->find(1);

            if (!$feature_features) {
                $regulation->retrieveFeatureFeatures();
            }
        }
    }

    
    /**
     * Empty all existing database tables containing objects from the store class
     *
     *
     *
     */
    public function dropRegulation()
    {

    }
}
