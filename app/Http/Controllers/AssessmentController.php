<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use App\Assessment;
use App\Report;

class AssessmentController extends Controller
{
    /**
     * Prepare the preliminary check
     *
     * @return \Illuminate\View\View
     */
    public function renderPreliminaryCheck()
    {
        $data = Assessment::initializeAssessment();

        return view('compliance.assessments.assessment.preliminary-check')->with('data', $data);
    }

    /**
     * Update a single preliminary answer in the database with
     * an AJAX post request defined in (assessment.js).
     *
     * @param \App\Assessment $assessnent
     * @return \Illuminate\Http\Response
     */
    public function updatePreliminaryCheck(Request $request, Assessment $assessment)
    {
        $response = $assessment->updatePreliminaryCheck($request->id, $request->answer);

        return Response::json($response);
    }

    /**
     * Update the preliminary status and submit the preliminary check
     *
     * @param \App\Assessment $assessment
     * @return \Illuminate\Http\Response
     */
    public function submitPreliminaryCheck(Assessment $assessment)
    {
        $response = $assessment->submitPreliminaryCheck();

        return Response::json($response);
    }

    /**
     * Reset the prelimary check including the preliminary status
     * and all associated answers given in the assessment.
     *
     * @param \App\Assessment $assessnent
     * @return \Illuminate\Http\Response
     */
    public function resetPreliminaryCheck(Assessment $assessment)
    {
        $response = $assessment->resetPreliminaryCheck();

        return Response::json($response);
    }

    /**
     * Prepare the chapters of the dashboard and check if a chapter contains applicable
     * duties to show it to the user. If no applicable duties are present, then the chapter
     * will be omitted in the dashboard and the side navigation.
     *
     * @return \Illuminate\View\View
     */
    public function renderDashboard()
    {
        $data = Assessment::initializeAssessment();

        return view('compliance.assessments.assessment.cockpit')->with('data', $data);
    }
}
