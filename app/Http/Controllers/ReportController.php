<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Assessment;
use App\Report;

class ReportController extends Controller
{
    /**
     * Call the method to render the report including all given answers.
     *
     * @return \Illuminate\View\View
     */
    public function renderReport()
    {
        $data = Assessment::initializeAssessment();

        if ($data['assessment_completion']) {
            return redirect('compliance/dashboard')->with('data', $data);
        }

        if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
            // Ignores notices and reports all other kinds... and warnings
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
        }

        $report = new Report($data);
        $report->collectData();
        $report->generateStats();

        $data['report'] = $report;

        return view('compliance.assessments.assessment.reports.report')->with('data', $data);
    }

    /**
     * Call the method to render the report including all given answers.
     *
     * @param \Request $request
     * @return \Illuminate\Http\Response
     */
    public function downloadReport(Request $request)
    {
        $report = new Report($request->chapter_content);
        $response = $report->createReport();

        return Response::json($response);
    }
}
