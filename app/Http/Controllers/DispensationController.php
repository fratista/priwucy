<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Dispensation;

class DispensationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Store the answer of the duty.
     *
     * @param  \Request  $request
     * @param  \Dispensation  $dispensation
     * @return \Illuminate\Http\Response
     */
    public function saveAnswer(Request $request, Dispensation $dispensation)
    {
        $response = $dispensation->saveAnswer($request);

        return Response::json($response);
    }

    /**
     * Delete the associated dependencies of answers when set to no.
     *
     * @param  \Request  $request
     * @param  \Dispensation  $dispensation
     * @return \Illuminate\Http\Response
     */
    public function deleteAnswers(Request $request, Dispensation $dispensation)
    {
        $response = $dispensation->deleteAnswers($request);

        return Response::json($response);
    }
}
