<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Assessment;
use App\Chapter;

class ChapterController extends Controller
{
    /**
     * Call the method to render the chapters including the duties, dispensations and features
     *
     * @param \App\Chapter $chapter_uri
     * @return \Illuminate\View\View
     */
    public function renderChapter(Request $request)
    {
        $data = Assessment::initializeAssessment();

        $chapter = Chapter::where('uri', 'http://privacylab.wu.ac.at/GDPR/' . $request->chapter)->first();
        $assessment_chapter = Chapter::getProgress($chapter->id);
        $chapter->status = $assessment_chapter['status'];
        $chapter->submission = $assessment_chapter['submission'];

        $data['duties'] = Chapter::renderChapter($chapter);
        $data['chapter'] = $chapter;

        return view('compliance.assessments.assessment.chapter')->with('data', $data);
    }

    /**
     * Update all answers for the chapter and the status as well
     *
     * @return \Response
     */
    public function updateChapter()
    {
        $response = Chapter::updateChapter($_POST['chapter_id'], $_POST['status']);

        return Response::json($response);
    }

    /**
     * Submit all stored answers for the specified chapter
     *
     * @param \App\Chapter $chapter_uri
     * @return \Response
     */
    public function submitChapter()
    {
        $response = Chapter::submitChapter($_POST['chapter_id']);

        return Response::json($response);
    }

    /**
     * Reset the stored answers for the specified chapter
     *
     * @param \App\Chapter $chapter_uri
     * @return \Response
     */
    public function resetChapter()
    {
        $response = Chapter::resetChapter($_POST['chapter_id']);

        return Response::json($response);
    }


    /**
     * Get the counts of the answered duties, features and dispensations
     * for the further processing of the report and detailed analysis
     *
     * @param \App\Chapter $chapter_uri
     * @return int
     */
    public static function getAnsweredDutyCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_duties = DB::table('assessment_duties')->where('assessment_id', $assessment_id)->count();

        return $answered_duties;
    }

    public static function getAnsweredFeatureCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_features = DB::table('assessment_features')->where('assessment_id', $assessment_id)->count();

        return $answered_features;
    }

    public static function getAnsweredDispensationCount()
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $answered_dispensations = DB::table('assessment_dispensations')->where('assessment_id', $assessment_id)->count();

        return $answered_dispensations;
    }
}
