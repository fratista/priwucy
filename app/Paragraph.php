<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paragraph extends Model
{
    public function __construct($paragraph = null, array $attributes = array())
    {
        parent::__construct($attributes);
    }
}
