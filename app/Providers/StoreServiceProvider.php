<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Store;

class StoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Store::class, function ($app) {
            return new Store();
        });
    }
}
