<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App;
use Cookie;
use Uuid;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Set the schema string length for the database
         * to avoid errors when executing database migrations.
         */
        Schema::defaultStringLength(191);

        /**
         * Generate a unique Uuid for identification of
         * user that are not registered to migrate the data
         * after a successful registration process.
         */
        if (!Cookie::get('uuid')) {
            $uuid = Uuid::generate();
            setcookie('uuid', $uuid, time() + (86400 * 30), '/');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
