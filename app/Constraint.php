<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constraint extends Model
{
    /**
     * This variables are used to store multiple questions
     * and answers for constraints if available.
     *
    */
    public $multiple_questions = [];
    public $multiple_answers = [];
    public $multiple_checked_yes = [];
    public $multiple_checked_no = [];
}
