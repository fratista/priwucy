<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Response;
use Session;

use Carbon\Carbon;

class Feature extends Model
{
    /**
     * Save the stored user anwer for a specific feature
     *
     * @param \Request $request
     * @return int
     */
    public function saveAnswer($request)
    {
        $duty_id = null;
        $dispensation_id = null;
        $dispensation_duty_id = null;
        $related_feature_id = null;
        $related_feature_duty_id = null;

        switch ($request->parent_type) {
             case 'feature':
                 $related_feature_id = $request->parent_id;
                 $related_feature_duty_id = $request->grandparent_id;
                 break;
             case 'dispensation':
                 $dispensation_id = $request->parent_id;
                 $dispensation_duty_id = $request->grandparent_id;
                 break;
             case 'duty':
                 $duty_id = $request->parent_id;
                 break;
         }

        $attachment = [
             'filename' => 'test',
             'mime' => 'jpeg2wbmp',
             'original_filename' => ''
         ];

        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        $assessment_feature = DB::table('assessment_features')
             ->where([
                 'feature_id' => $request->feature_id,
                 'assessment_id' => $assessment_id
             ])->first();

        /**
         * Option to answer each question separately. First the query
         * checks if there is an answer with the given signature (parent_id, granparent_id).
         * If none is available it will be created in addition to the answers of the same
         * feature that were provided in different questions. This will only be done once
         * it is set in the assessment configuration.
         *
         */
        if (config('assessment.answer_mode') == 'individual') {
            $assessment_feature = DB::table('assessment_features')
                 ->where([
                     'feature_id' => $request->feature_id,
                     'duty_id' => $duty_id,
                     'dispensation_id' => $dispensation_id,
                     'dispensation_duty_id' => $dispensation_duty_id,
                     'related_feature_id' => $related_feature_id,
                     'related_feature_duty_id' => $related_feature_duty_id,
                     'assessment_id' => $assessment_id
                 ])->first();
        }

        if ($assessment_feature) {
            DB::table('assessment_features')
                 ->where([
                     'feature_id' => $request->feature_id,
                     'duty_id' => $duty_id,
                     'dispensation_id' => $dispensation_id,
                     'dispensation_duty_id' => $dispensation_duty_id,
                     'related_feature_id' => $related_feature_id,
                     'related_feature_duty_id' => $related_feature_duty_id,
                     'assessment_id' => $assessment_id
                 ])
                 ->update([
                     'answer' => $request->answer,
                     'comment' => $request->comment,
                     'filename' => $attachment['filename'],
                     'mime' => $attachment['mime'],
                     'original_filename' => $attachment['original_filename'],
                     'updated_at' => Carbon::now()
                 ]);

            return 2;
        } else {
            DB::table('assessment_features')->insert([
                 'answer' => $request->answer,
                 'comment' => $request->comment,
                 'filename' => $attachment['filename'],
                 'mime' => $attachment['mime'],
                 'original_filename' => $attachment['original_filename'],
                 'feature_id' => $request->feature_id,
                 'duty_id' => $duty_id,
                 'dispensation_id' => $dispensation_id,
                 'dispensation_duty_id' => $dispensation_duty_id,
                 'related_feature_id' => $related_feature_id,
                 'related_feature_duty_id' => $related_feature_duty_id,
                 'assessment_id' => $assessment_id,
                 'created_at' => Carbon::now()
             ]);

            return 1;
        }

        return 0;
    }

    /**
     * Return the stored user anwer for a specific feature
     *
     * @param $feature_id, $parent_id, $grandparent_id
     * @return $assessment_feature
     */
    public static function getAnswer($feature_id, $parent_id = null, $grandparent_id = null)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;
        $assessment_feature = DB::table('assessment_features')
             ->where([
                 'feature_id' => $feature_id,
                 'assessment_id' => $assessment_id
             ])->first();

        if (config('assessment.answer_mode') == 'individual') {
        }

        if (!$assessment_feature) {
            $assessment_feature = collect();
            $assessment_feature->answer = 3;
            $assessment_feature->comment = '';
            $assessment_feature->original_filename = 'No file selected';
            $assessment_feature->duty_id = null;
            $assessment_feature->dispensation_id = null;
            $assessment_feature->dispensation_duty_id = null;
            $assessment_feature->related_feature_id = null;
            $assessment_feature->related_feature_duty_id = null;
        }

        return $assessment_feature;
    }

    /**
     * Delete the associated dependencies of answers when set to no.
     *
     * @param @param \Request $request
     * @return int
     */
    public function deleteAnswers($request)
    {
        $assessment_id = Assessment::where('user_id', Auth::id())->first()->id;

        switch ($request->parent_type) {
             case 'feature':
                $assessment_feature = DB::table('assessment_features')
                     ->where([
                         'related_feature_id' => $request->parent_id,
                         'related_feature_duty_id' => $request->grandparent_id,
                         'assessment_id' => $assessment_id
                     ])->delete();

                 break;
             case 'related_feature':
                 $assessment_feature = DB::table('assessment_features')
                     ->where([
                         'related_feature_duty_id' => $request->parent_id,
                         'assessment_id' => $assessment_id
                     ])->delete();

                 break;
             case 'dispensation':
                 $assessment_feature = DB::table('assessment_features')
                     ->where([
                         'dispensation_id' => $request->parent_id,
                         'dispensation_duty_id' => $request->grandparent_id,
                         'assessment_id' => $assessment_id
                     ])->delete();

                 break;
            case 'dispensation_feature':
                $assessment_feature = DB::table('assessment_features')
                    ->where([
                        'dispensation_duty_id' => $request->grandparent_id,
                        'assessment_id' => $assessment_id
                    ])->delete();

                break;
             case 'duty':
                 $assessment_feature = DB::table('assessment_features')
                     ->where([
                         'duty_id' => $request->parent_id,
                         'assessment_id' => $assessment_id
                     ])->delete();

                 break;
         }

        return 1;
    }
}
