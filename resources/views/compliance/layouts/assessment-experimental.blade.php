<div class="container-fluid">
    <div class="row">
        <nav id="sidebar" class="px-0 bg-dark bg-gradient sidebar mCustomScrollbar _mCS_2 mCS-autoHide mCS_no_scrollbar" style="height: 1297px;">
            <button class="hamburger hamburger--slider" type="button" data-target=".sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle Sidebar">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
            <div id="mCSB_2" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" tabindex="0" style="max-height: none;">
                <div id="mCSB_2_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
    				<ul class="nav nav-pills flex-column">
    					<li class="logo-nav-item">
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <img src="{{ asset('img/logo.png') }}" height="25" alt="GDPR Assessment">
                            </a>
    					</li>
    					<li>
    						<h6 class="nav-header">General</h6>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="index.html">
    							<i class="batch-icon batch-icon-browser-alt"></i>
    							Dashboard <span class="sr-only">(current)</span>
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-layout-content-left"></i>
    							Layout
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="layout-left-menu-hidden.html">Left Menu - Hidden</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="layout-left-menu-normal.html">Left Menu - Normal</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="layout-top-menu-fixed.html">Top Menu - Fixed</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="layout-top-menu-normal.html">Top Menu - Normal</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="starter-kit.html">
    							<i class="batch-icon batch-icon-star"></i>
    							Starter Kit
    						</a>
    					</li>
    				</ul>

    				<ul class="nav nav-pills flex-column">
    					<li>
    						<h6 class="nav-header">Apps</h6>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-store"></i>
    							eCommerce
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="ecommerce-dashboard.html">Dashboard</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="ecommerce-product-page.html">Product Page</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="ecommerce-category-page.html">Category Page</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="ecommerce-cart.html">Cart</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="ecommerce-checkout-page.html">Checkout Page</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-search"></i>
    							Search Results
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="search-results-normal.html">Normal Search</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="search-results-media.html">Media Search</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-mail"></i>
    							Mail
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="mail-inbox.html">Inbox</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="mail-message.html">Message</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="mail-compose.html">Compose</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="calendar.html">
    							<i class="batch-icon batch-icon-calendar"></i>
    							Calendar
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-list"></i>
    							Tasks
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="task-list.html">Task List</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="task-manager.html">Task Manager</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="timeline.html">
    							<i class="batch-icon batch-icon-timeline"></i>
    							Timeline
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="gallery.html">
    							<i class="batch-icon batch-icon-image"></i>
    							Gallery <span class="badge badge-danger">New *</span>
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link active" href="price-list.html">
    							<i class="batch-icon batch-icon-tag-alt-2"></i>
    							Price List
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="invoice.html">
    							<i class="batch-icon batch-icon-list-alt"></i>
    							Invoice
    						</a>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-browser-alt-close"></i>
    							Error Pages
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="error-pages-404.html">404 Page</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="error-pages-500.html">500 Page</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link nav-parent" href="#">
    							<i class="batch-icon batch-icon-locked"></i>
    							Sign In &amp; Sign Up
    						</a>
    						<ul class="nav nav-pills flex-column">
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-signin.html">Sign In</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-signin-2.html">Sign In 2</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-signup.html">Sign Up</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-signup-2.html">Sign Up 2</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-forgot-password.html">Forgot Password</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-forgot-password-2.html">Forgot Password 2</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-lock-screen.html">Lock Screen</a>
    							</li>
    							<li class="nav-item">
    								<a class="nav-link" href="sisu-lock-screen-2.html">Lock Screen 2</a>
    							</li>
    						</ul>
    					</li>
    					<li class="nav-item">
    						<a class="nav-link" href="profiles-member-profile.html">
    							<i class="batch-icon batch-icon-users"></i>
    							Profile
    						</a>
    					</li>
    				</ul>
                </div>

                <div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-light mCSB_scrollTools_vertical mCSB_scrollTools_onDrag_expand" style="display: none;">
                    <div class="mCSB_draggerContainer">
                        <div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;">
                            <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                        </div>
                        <div class="mCSB_draggerRail"></div>
                    </div>
                </div>
            </div>
        </nav>

        <div class="right-column">
				<nav class="navbar navbar-expand-lg navbar-light bg-white">
					<a class="navbar-brand d-block d-sm-block d-md-block d-lg-none" href="#">
						<img src="assets/img/logo-dark.png" width="145" height="32.3" alt="QuillPro">
					</a>
					<button class="hamburger hamburger--slider" type="button" data-target=".sidebar" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle Sidebar">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
					<!-- Added Mobile-Only Menu -->
					<ul class="navbar-nav ml-auto mobile-only-control d-block d-sm-block d-md-block d-lg-none">
						<li class="nav-item dropdown" style="position: static;">
							<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbar-notification-search-mobile" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
								<i class="batch-icon batch-icon-search"></i>
							</a>
							<ul class="dropdown-menu dropdown-menu-fullscreen" aria-labelledby="navbar-notification-search-mobile" style="width: 1086px;">
								<li>
									<form class="form-inline my-2 my-lg-0 no-waves-effect">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Search for..." aria-label="Search for..." aria-describedby="basic-addon2">
											<div class="input-group-append">
												<button class="btn btn-primary btn-gradient waves-effect waves-light" type="button"><span class="gradient">
													<i class="batch-icon batch-icon-search"></i>
												</span></button>
											</div>
										</div>
									</form>
								</li>
							</ul>
						</li>
					</ul>

					<!--  DEPRECATED CODE:
						<div class="navbar-collapse" id="navbarSupportedContent">
					-->
					<!-- USE THIS CODE Instead of the Commented Code Above -->
					<!-- .collapse added to the element -->
					<div class="collapse navbar-collapse" id="navbar-header-content">
						<ul class="navbar-nav navbar-language-translation mr-auto">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbar-dropdown-menu-link" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<i class="batch-icon batch-icon-book-alt-"></i>
									English
								</a>
								<ul class="dropdown-menu" aria-labelledby="navbar-dropdown-menu-link">
									<li><a class="dropdown-item waves-effect waves-light" href="#">Français</a></li>
									<li><a class="dropdown-item waves-effect waves-light" href="#">Deutsche</a></li>
									<li><a class="dropdown-item waves-effect waves-light" href="#">Español</a></li>
								</ul>
							</li>
						</ul>
						<ul class="navbar-nav navbar-notifications float-right">
							<li class="nav-item dropdown" style="position: static;">
								<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbar-notification-search" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<i class="batch-icon batch-icon-search"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-fullscreen" aria-labelledby="navbar-notification-search" style="width: 1086px;">
									<li>
										<form class="form-inline my-2 my-lg-0 no-waves-effect">
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Search for..." aria-label="Search for..." aria-describedby="basic-addon2">
												<div class="input-group-append">
													<button class="btn btn-primary btn-gradient waves-effect waves-light" type="button"><span class="gradient">Search</span></button>
												</div>
											</div>
										</form>
									</li>
								</ul>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle no-waves-effect" id="navbar-notification-calendar" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<i class="batch-icon batch-icon-calendar"></i>
									<span class="notification-number">6</span>
								</a>
								<ul class="dropdown-menu dropdown-menu-right dropdown-menu-md" aria-labelledby="navbar-notification-calendar">
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-calendar batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Meeting with Project Manager</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">Right now</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-calendar batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Sales Call</h6>
												<div class="notification-text">
													Nibh amet cras sit libero
												</div>
												<span class="notification-time">One hour from now</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-calendar batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Email CEO new expansion proposal</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">In 3 days</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-calendar batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Team building exercise</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">In one week</span>
											</div>
										</a>
									</li>
								</ul>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle no-waves-effect" id="navbar-notification-misc" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<i class="batch-icon batch-icon-bell"></i>
									<span class="notification-number">4</span>
								</a>
								<ul class="dropdown-menu dropdown-menu-right dropdown-menu-md" aria-labelledby="navbar-notification-misc">
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-bell batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">General Notification</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">Just now</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-cloud-download batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Your Download Is Ready</h6>
												<div class="notification-text">
													Nibh amet cras sit libero
												</div>
												<span class="notification-time">5 minutes ago</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-tag-alt-2 batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">New Order</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">Yesterday</span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="task-list.html" class=" waves-effect waves-light">
											<i class="batch-icon batch-icon-pull batch-icon-xl d-flex mr-3"></i>
											<div class="media-body">
												<h6 class="mt-0 mb-1 notification-heading">Pull Request</h6>
												<div class="notification-text">
													Cras sit amet nibh libero
												</div>
												<span class="notification-time">3 day ago</span>
											</div>
										</a>
									</li>
								</ul>
							</li>
						</ul>
						<ul class="navbar-nav ml-5 navbar-profile">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle waves-effect waves-light" id="navbar-dropdown-navbar-profile" data-toggle="dropdown" data-flip="false" aria-haspopup="true" aria-expanded="false">
									<div class="profile-name">
										Johanna Quinn
									</div>
									<div class="profile-picture bg-gradient bg-primary has-message float-right">
										<img src="assets/img/profile-pic.jpg" width="44" height="44">
									</div>
								</a>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbar-dropdown-navbar-profile">
									<li><a class="dropdown-item waves-effect waves-light" href="profiles-member-profile.html">Profile</a></li>
									<li>
										<a class="dropdown-item waves-effect waves-light" href="mail-inbox.html">
											Messages
											<span class="badge badge-danger badge-pill float-right">3</span>
										</a>
									</li>
									<li><a class="dropdown-item waves-effect waves-light" href="profiles-member-profile.html">Settings</a></li>
									<li><a class="dropdown-item waves-effect waves-light" href="sisu-lock-screen.html">Logout</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
				<main class="main-content p-5" role="main">
					<div class="row">
						<div class="col-md-12">
							<h1>Calendar</h1>
						</div>
					</div>
					<div class="row mb-5">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">
									<div class="calendar-container">
										<div class="row">
											<div class="col-lg-4">
												<div class="calendar-controls">
													<div class="create-event">
														<h3>1) Create An Event...</h3>
														<div class="input-group">
															<input type="text" class="form-control" id="input-new-event" placeholder="Enter Your Event Name..." aria-label="Enter Your Event Name...">
															<div class="input-group-append">
																<button type="button" id="add-available-event" class="btn btn-primary waves-effect waves-light">
																	<span class="sr-only">Add Event</span>
																	<i class="batch-icon batch-icon-plus"></i>
																</button>
																<button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split waves-effect waves-light highlight-color-blue" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<i class="batch-icon batch-icon-droplet"></i>
																	<span class="sr-only">Toggle Color Dropdown</span>
																</button>
																<div class="dropdown-menu dropdown-menu-right">
																	<li class="dropdown-item legend-block-item active">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-blue" data-event-color="blue">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																	<li class="dropdown-item legend-block-item">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-green" data-event-color="green">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																	<li class="dropdown-item legend-block-item">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-red" data-event-color="red">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																	<li class="dropdown-item legend-block-item">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-yellow" data-event-color="yellow">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																	<li class="dropdown-item legend-block-item">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-orange" data-event-color="orange">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																	<li class="dropdown-item legend-block-item">
																		<div class="legend-block-color">
																			<div class="legend-block-color-box highlight-color-purple" data-event-color="purple">
																				<i class="batch-icon batch-icon-droplet"></i>
																			</div>
																		</div>
																	</li>
																</div>
															</div>
														</div>
														<span class="help-block">Use the Dropdown menu to select your Event Color</span>
													</div>
													<div class="event-preview mt-3">
														<h6>Event Preview</h6>
														<div class="event-preview-item">
															<div class="legend-block-item">
																<div class="legend-block-color">
																	<div class="legend-block-color-box highlight-color-blue" data-event-color="orange">
																		<i class="batch-icon batch-icon-droplet"></i>
																	</div>
																</div>
																<div class="legend-block-text">
																	<div id="preview-event-name">Your Event Name</div>
																</div>
															</div>
														</div>
													</div>
													<div class="available-events mt-5">
														<h3>2) ...Then, Drag It To The Calendar</h3>
														<div class="event-list">
															<div class="fc-event ui-draggable ui-draggable-handle">
																<div class="legend-block-item">
																	<div class="legend-block-color">
																		<div class="legend-block-color-box highlight-color-blue" data-event-color="blue">
																			<i class="batch-icon batch-icon-droplet"></i>
																		</div>
																	</div>
																	<div class="legend-block-text">Date Night</div>
																</div>
															</div>
															<div class="fc-event ui-draggable ui-draggable-handle">
																<div class="legend-block-item">
																	<div class="legend-block-color">
																		<div class="legend-block-color-box highlight-color-red" data-event-color="red">
																			<i class="batch-icon batch-icon-droplet"></i>
																		</div>
																	</div>
																	<div class="legend-block-text">Meeting With Coca-Cola</div>
																</div>
															</div>
															<div class="fc-event ui-draggable ui-draggable-handle">
																<div class="legend-block-item">
																	<div class="legend-block-color">
																		<div class="legend-block-color-box highlight-color-yellow" data-event-color="yellow">
																			<i class="batch-icon batch-icon-droplet"></i>
																		</div>
																	</div>
																	<div class="legend-block-text">Pay Water Bill</div>
																</div>
															</div>
															<div class="empty-list">
																There are currently no events to add. Create one above.
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-8">
												<div id="calendar" class="fc fc-bootstrap3 fc-ltr"><div class="fc-toolbar fc-header-toolbar"><div class="fc-left"><div class="btn-group"><button type="button" class="fc-prev-button btn btn-default"><span class="batch-icon batch-icon-arrow-left"></span></button><button type="button" class="fc-next-button btn btn-default"><span class="batch-icon batch-icon-arrow-right"></span></button></div><button type="button" class="fc-today-button btn btn-default">today</button></div><div class="fc-right"><div class="btn-group"><button type="button" class="fc-month-button btn btn-default active">month</button><button type="button" class="fc-agendaWeek-button btn btn-default ">week</button><button type="button" class="fc-agendaDay-button btn btn-default ">day</button></div></div><div class="fc-center"><h2>November 2017</h2></div><div class="fc-clear"></div></div><div class="fc-view-container" style=""><div class="fc-view fc-month-view fc-basic-view" style=""><table class="table-bordered"><thead class="fc-head"><tr><td class="fc-head-container "><div class="fc-row panel-default"><table class="table-bordered"><thead><tr><th class="fc-day-header  fc-sun"><span>Sun</span></th><th class="fc-day-header  fc-mon"><span>Mon</span></th><th class="fc-day-header  fc-tue"><span>Tue</span></th><th class="fc-day-header  fc-wed"><span>Wed</span></th><th class="fc-day-header  fc-thu"><span>Thu</span></th><th class="fc-day-header  fc-fri"><span>Fri</span></th><th class="fc-day-header  fc-sat"><span>Sat</span></th></tr></thead></table></div></td></tr></thead><tbody class="fc-body"><tr><td class=""><div class="fc-scroller fc-day-grid-container" style="overflow: hidden; height: 446px;"><div class="fc-day-grid fc-unselectable"><div class="fc-row fc-week panel-default fc-rigid" style="height: 74px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-other-month fc-past" data-date="2017-10-29"></td><td class="fc-day  fc-mon fc-other-month fc-past" data-date="2017-10-30"></td><td class="fc-day  fc-tue fc-other-month fc-past" data-date="2017-10-31"></td><td class="fc-day  fc-wed fc-past" data-date="2017-11-01"></td><td class="fc-day  fc-thu fc-past" data-date="2017-11-02"></td><td class="fc-day  fc-fri fc-past" data-date="2017-11-03"></td><td class="fc-day  fc-sat fc-past" data-date="2017-11-04"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-other-month fc-past" data-date="2017-10-29"><span class="fc-day-number">29</span></td><td class="fc-day-top fc-mon fc-other-month fc-past" data-date="2017-10-30"><span class="fc-day-number">30</span></td><td class="fc-day-top fc-tue fc-other-month fc-past" data-date="2017-10-31"><span class="fc-day-number">31</span></td><td class="fc-day-top fc-wed fc-past" data-date="2017-11-01"><span class="fc-day-number">1</span></td><td class="fc-day-top fc-thu fc-past" data-date="2017-11-02"><span class="fc-day-number">2</span></td><td class="fc-day-top fc-fri fc-past" data-date="2017-11-03"><span class="fc-day-number">3</span></td><td class="fc-day-top fc-sat fc-past" data-date="2017-11-04"><span class="fc-day-number">4</span></td></tr></thead><tbody><tr><td></td><td></td><td></td><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-red fc-draggable fc-resizable"><div class="fc-content"> <span class="fc-title">All Day Event</span></div><div class="fc-resizer fc-end-resizer"></div></a></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week panel-default fc-rigid" style="height: 74px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-past" data-date="2017-11-05"></td><td class="fc-day  fc-mon fc-past" data-date="2017-11-06"></td><td class="fc-day  fc-tue fc-past" data-date="2017-11-07"></td><td class="fc-day  fc-wed fc-past" data-date="2017-11-08"></td><td class="fc-day  fc-thu fc-past" data-date="2017-11-09"></td><td class="fc-day  fc-fri fc-past" data-date="2017-11-10"></td><td class="fc-day  fc-sat fc-past" data-date="2017-11-11"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-past" data-date="2017-11-05"><span class="fc-day-number">5</span></td><td class="fc-day-top fc-mon fc-past" data-date="2017-11-06"><span class="fc-day-number">6</span></td><td class="fc-day-top fc-tue fc-past" data-date="2017-11-07"><span class="fc-day-number">7</span></td><td class="fc-day-top fc-wed fc-past" data-date="2017-11-08"><span class="fc-day-number">8</span></td><td class="fc-day-top fc-thu fc-past" data-date="2017-11-09"><span class="fc-day-number">9</span></td><td class="fc-day-top fc-fri fc-past" data-date="2017-11-10"><span class="fc-day-number">10</span></td><td class="fc-day-top fc-sat fc-past" data-date="2017-11-11"><span class="fc-day-number">11</span></td></tr></thead><tbody><tr><td rowspan="2"></td><td rowspan="2"></td><td class="fc-event-container" colspan="3"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-yellow fc-draggable fc-resizable"><div class="fc-content"> <span class="fc-title">Long Event</span></div><div class="fc-resizer fc-end-resizer"></div></a></td><td rowspan="2"></td><td class="fc-event-container" rowspan="2"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-not-end highlight-color-green fc-draggable"><div class="fc-content"> <span class="fc-title">Conference</span></div></a></td></tr><tr><td></td><td></td><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable" style="background-color:#ff0097;border-color:#ff0097"><div class="fc-content"><span class="fc-time">4p</span> <span class="fc-title">Repeating Event</span></div></a></td></tr></tbody></table></div></div><div class="fc-row fc-week panel-default fc-rigid" style="height: 74px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-past" data-date="2017-11-12"></td><td class="fc-day  fc-mon fc-past" data-date="2017-11-13"></td><td class="fc-day  fc-tue fc-past" data-date="2017-11-14"></td><td class="fc-day  fc-wed fc-past" data-date="2017-11-15"></td><td class="fc-day  fc-thu fc-past" data-date="2017-11-16"></td><td class="fc-day  fc-fri fc-past" data-date="2017-11-17"></td><td class="fc-day  fc-sat fc-past" data-date="2017-11-18"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-past" data-date="2017-11-12"><span class="fc-day-number">12</span></td><td class="fc-day-top fc-mon fc-past" data-date="2017-11-13"><span class="fc-day-number">13</span></td><td class="fc-day-top fc-tue fc-past" data-date="2017-11-14"><span class="fc-day-number">14</span></td><td class="fc-day-top fc-wed fc-past" data-date="2017-11-15"><span class="fc-day-number">15</span></td><td class="fc-day-top fc-thu fc-past" data-date="2017-11-16"><span class="fc-day-number">16</span></td><td class="fc-day-top fc-fri fc-past" data-date="2017-11-17"><span class="fc-day-number">17</span></td><td class="fc-day-top fc-sat fc-past" data-date="2017-11-18"><span class="fc-day-number">18</span></td></tr></thead><tbody><tr><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-not-start fc-end highlight-color-green fc-draggable fc-resizable"><div class="fc-content"> <span class="fc-title">Conference</span></div><div class="fc-resizer fc-end-resizer"></div></a></td><td class="fc-event-container" rowspan="6"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable"><div class="fc-content"><span class="fc-time">7a</span> <span class="fc-title">Birthday Party</span></div></a></td><td rowspan="6"></td><td rowspan="6"></td><td class="fc-event-container" rowspan="6"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-purple fc-draggable"><div class="fc-content"><span class="fc-time">4p</span> <span class="fc-title">Repeating Event</span></div></a></td><td rowspan="6"></td><td rowspan="6"></td></tr><tr><td class="fc-event-container fc-limited"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-green fc-draggable"><div class="fc-content"><span class="fc-time">10:30a</span> <span class="fc-title">Meeting</span></div></a></td><td class="fc-more-cell" rowspan="1"><div><a class="fc-more">+5 more</a></div></td></tr><tr class="fc-limited"><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable" style="background-color:#6ec06e;border-color:#6ec06e"><div class="fc-content"><span class="fc-time">12p</span> <span class="fc-title">Lunch</span></div></a></td></tr><tr class="fc-limited"><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-red fc-draggable"><div class="fc-content"><span class="fc-time">2:30p</span> <span class="fc-title">Meeting</span></div></a></td></tr><tr class="fc-limited"><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-red fc-draggable"><div class="fc-content"><span class="fc-time">5:30p</span> <span class="fc-title">Happy Hour</span></div></a></td></tr><tr class="fc-limited"><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end highlight-color-blue fc-draggable"><div class="fc-content"><span class="fc-time">8p</span> <span class="fc-title">Dinner</span></div></a></td></tr></tbody></table></div></div><div class="fc-row fc-week panel-default fc-rigid" style="height: 74px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-past" data-date="2017-11-19"></td><td class="fc-day  fc-mon fc-past" data-date="2017-11-20"></td><td class="fc-day  fc-tue fc-past" data-date="2017-11-21"></td><td class="fc-day  fc-wed fc-past" data-date="2017-11-22"></td><td class="fc-day  fc-thu fc-past" data-date="2017-11-23"></td><td class="fc-day  fc-fri fc-past" data-date="2017-11-24"></td><td class="fc-day  fc-sat fc-past" data-date="2017-11-25"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-past" data-date="2017-11-19"><span class="fc-day-number">19</span></td><td class="fc-day-top fc-mon fc-past" data-date="2017-11-20"><span class="fc-day-number">20</span></td><td class="fc-day-top fc-tue fc-past" data-date="2017-11-21"><span class="fc-day-number">21</span></td><td class="fc-day-top fc-wed fc-past" data-date="2017-11-22"><span class="fc-day-number">22</span></td><td class="fc-day-top fc-thu fc-past" data-date="2017-11-23"><span class="fc-day-number">23</span></td><td class="fc-day-top fc-fri fc-past" data-date="2017-11-24"><span class="fc-day-number">24</span></td><td class="fc-day-top fc-sat fc-past" data-date="2017-11-25"><span class="fc-day-number">25</span></td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week panel-default fc-rigid" style="height: 74px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-past" data-date="2017-11-26"></td><td class="fc-day  fc-mon fc-past" data-date="2017-11-27"></td><td class="fc-day  fc-tue fc-past" data-date="2017-11-28"></td><td class="fc-day  fc-wed fc-past" data-date="2017-11-29"></td><td class="fc-day  fc-thu fc-past" data-date="2017-11-30"></td><td class="fc-day  fc-fri fc-other-month fc-past" data-date="2017-12-01"></td><td class="fc-day  fc-sat fc-other-month fc-past" data-date="2017-12-02"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-past" data-date="2017-11-26"><span class="fc-day-number">26</span></td><td class="fc-day-top fc-mon fc-past" data-date="2017-11-27"><span class="fc-day-number">27</span></td><td class="fc-day-top fc-tue fc-past" data-date="2017-11-28"><span class="fc-day-number">28</span></td><td class="fc-day-top fc-wed fc-past" data-date="2017-11-29"><span class="fc-day-number">29</span></td><td class="fc-day-top fc-thu fc-past" data-date="2017-11-30"><span class="fc-day-number">30</span></td><td class="fc-day-top fc-fri fc-other-month fc-past" data-date="2017-12-01"><span class="fc-day-number">1</span></td><td class="fc-day-top fc-sat fc-other-month fc-past" data-date="2017-12-02"><span class="fc-day-number">2</span></td></tr></thead><tbody><tr><td></td><td></td><td class="fc-event-container"><a class="fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable fc-resizable" href="https://base5builder.com/"><div class="fc-content"> <span class="fc-title">Click for Google</span></div><div class="fc-resizer fc-end-resizer"></div></a></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week panel-default fc-rigid" style="height: 76px;"><div class="fc-bg"><table class="table-bordered"><tbody><tr><td class="fc-day  fc-sun fc-other-month fc-past" data-date="2017-12-03"></td><td class="fc-day  fc-mon fc-other-month fc-past" data-date="2017-12-04"></td><td class="fc-day  fc-tue fc-other-month fc-past" data-date="2017-12-05"></td><td class="fc-day  fc-wed fc-other-month fc-past" data-date="2017-12-06"></td><td class="fc-day  fc-thu fc-other-month fc-past" data-date="2017-12-07"></td><td class="fc-day  fc-fri fc-other-month fc-past" data-date="2017-12-08"></td><td class="fc-day  fc-sat fc-other-month fc-past" data-date="2017-12-09"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-top fc-sun fc-other-month fc-past" data-date="2017-12-03"><span class="fc-day-number">3</span></td><td class="fc-day-top fc-mon fc-other-month fc-past" data-date="2017-12-04"><span class="fc-day-number">4</span></td><td class="fc-day-top fc-tue fc-other-month fc-past" data-date="2017-12-05"><span class="fc-day-number">5</span></td><td class="fc-day-top fc-wed fc-other-month fc-past" data-date="2017-12-06"><span class="fc-day-number">6</span></td><td class="fc-day-top fc-thu fc-other-month fc-past" data-date="2017-12-07"><span class="fc-day-number">7</span></td><td class="fc-day-top fc-fri fc-other-month fc-past" data-date="2017-12-08"><span class="fc-day-number">8</span></td><td class="fc-day-top fc-sat fc-other-month fc-past" data-date="2017-12-09"><span class="fc-day-number">9</span></td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></div></div></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</main>
			</div>
    </div>
</div>
