@extends('skeleton')

@section('layout')

@include('compliance.layouts.assessment-old')

<header class="header dashboard-header flex-center"></header>
<section class="container-fluid assessment-wrapper">
    <div class="row flex-xl-nowrap">
        <div class="col-12 col-lg-3 col-xl-4 bd-sidebar">
            <nav class="dashboard-menu-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navDashBoard" aria-controls="navDashBoard" aria-expanded="false" aria-label="Toggle navigation" id="toggle-dashboard-nav">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container dashboard-menu-container collapse d-lg-block" id="navDashBoard">
                    <div class="dashboard-menu">
                        <ul>
                            <li>
                                <i class="fa fa-question" aria-hidden="true"></i>
                                <a href="/assessment/preliminary-check">Preliminary Check</a>
                            </li>
                            <li @if (!$data['preliminary_progress']) class="pri-cursor" @endif>
                                <i class="fa fa-tachometer" aria-hidden="true"></i>
                                <a href="/assessment/dashboard" @if (!$data['preliminary_progress']) class="pri-disabled" @endif>GDPR Compliance Dashboard</a>
                                <ul class="nav-chapter">
                                    @foreach ($data['chapters'] as $chapter)
                                        @if ($data['preliminary_progress'] == 1)
                                            @if (1)
                                                <li class="show-chapter {{ $chapter->cursor }}">
                                                    <i class="fa {{ App\Helper::menuIcons($chapter->title) }}" aria-hidden="true"></i>
                                                    <a class="btn-show-duties {{ $chapter->disabled }}" href="/assessment/chapters/{{ App\Helper::extractName($chapter->uri) }}">{{ $chapter->title }}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li class="pri-cursor">
                                                <i class="fa {{ App\Helper::menuIcons($chapter->title) }}" aria-hidden="true"></i>
                                                <a class="pri-disabled">{{ $chapter->title }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                            <li class="pri-cursor">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                                <a href="/assessment/report" @if ($data['assessment_completion']) class="pri-disabled" @endif>Report Generation</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="col-12 col-lg-9 col-xl-8" id="assessment-container">
            <div class="container dashboard-container">
                <div class="row">
                    <header class="item-title dashboard-active">
                        <h3>
                            @yield('title')
                        </h3>
                        <i class="fa fa-info" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="@yield('information-title')"></i>
                    </header>
                </div>
                <div class="row">
                    @yield('subpart')
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
