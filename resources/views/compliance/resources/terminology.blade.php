@extends('compliance.layouts.assessment-old', ['data' => $data])

@section('title', 'Terminology')
@section('information-title', 'These are the definitions for the used expressions.')

@section('subpart')

@php
$language = 'EN';
$keywords = ['test', 'test']
@endphp

<div class="col-sm-6 offset-md-3">
    <p class="text-center">Before you go through the survey you can find the most relevant terms and definitions in this section. Choose a language option to show it in the desired language.</p>
</div>
<div class="col-2 offset-md-5">
    <select class="form-control language-option">
        <!--<option val="BG" <?php if ($language == 'BG') echo 'selected'; ?>>BG</option>-->
        <option val="BG" @if ($language == 'BG') selected @endif >BG</option>
        <option val="ES" @if ($language == 'ES') selected @endif >ES</option>
        <option val="CS" @if ($language == 'CS') selected @endif >CS</option>
        <option val="DA" @if ($language == 'DA') selected @endif >DA</option>
        <option val="DE" @if ($language == 'DE') selected @endif >DE</option>
        <option val="ET" @if ($language == 'ET') selected @endif >ET</option>
        <option val="EN" @if ($language == 'EN') selected @endif >EN</option>
        <option val="FR" @if ($language == 'FR') selected @endif >FR</option>
        <option val="GA" @if ($language == 'GA') selected @endif >GA</option>
        <option val="HR" @if ($language == 'HR') selected @endif >HR</option>
        <option val="IT" @if ($language == 'IT') selected @endif >IT</option>
        <option val="LV" @if ($language == 'LV') selected @endif >LV</option>
        <option val="LT" @if ($language == 'LT') selected @endif >LT</option>
        <option val="HU" @if ($language == 'HU') selected @endif >HU</option>
        <option val="MT" @if ($language == 'MT') selected @endif >MT</option>
        <option val="NL" @if ($language == 'NL') selected @endif >NL</option>
        <option val="PL" @if ($language == 'PL') selected @endif >PL</option>
        <option val="PT" @if ($language == 'PT') selected @endif >PT</option>
        <option val="RO" @if ($language == 'RO') selected @endif >RO</option>
        <option val="SK" @if ($language == 'SK') selected @endif >SK</option>
        <option val="SL" @if ($language == 'SL') selected @endif >SL</option>
        <option val="FI" @if ($language == 'FI') selected @endif >FI</option>
        <option val="SV" @if ($language == 'SV') selected @endif >SV</option>
    </select>
</div>

<div class="col-sm-8 offset-sm-2">
@foreach ($keywords as $keyword=>$definition)
    <div class="definition-container">
        <div class="container question item-title">{{ $keyword }}</div>
        <div class="definition-information">{{ $definition }}</div>
    </div>
@endforeach

    <p class="text-xs-center" style="text-align:center">To proceed, you have to answer some questions in advance. Based on the answers, a different progress will be shown.</p>
    <a href="/assessment/preliminary-check" class="btn btn-info btn-submit start-preliminary">Start Preliminary Check</a>
</div>

@endsection
