@extends('compliance.layouts.assessment-old', ['data' => $data])

@section('title', 'Preliminary Check')
@section('information-title', 'This is the preliminary check. The questions will determine which duties are applicable.')

@section('subpart')

<form id="form-preliminary">

@if ($data['preliminary_progress'])
    <div class="col-sm-10 offset-sm-1 col-lg-8 offset-lg-2">
        <p class="text-center">You have already answered the following questions. Please press the reset button, in order to change the answers. Pressing reset will also reset the GDPR Dashboard.</p>
        <button class="btn btn-info btn-submit btn-reset reset-preliminary" id="reset-preliminary">Reset</button>
    </div>
@endif

@foreach ($data['constraints'] as $constraint)
    <div class="item-container">
        <div class="item-title">
            <span>{{ $constraint->title }}</span>
            <i class="fa fa-info" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ $constraint->information }}"></i>
        </div>

        @if (count($constraint->multiple_questions) > 0)
            @for ($i = 0; $i < count($constraint->multiple_questions); $i++)
                <div class="item-question">
                    <span>{{ $constraint->multiple_questions[$i] }}</span>
                </div>
                <div class="item-answers multiple-question {{ $data['cursor'] }}">
                    <label class="radio-label-preliminary radio-label {{ $data['disabled'] }}">
                        <input type="radio" class="radio-preliminary {{ $constraint->multiple_checked_yes[$i] }}" name="radio-y-{{ $i . '-' . $constraint->id }}" data-id="{{ $constraint->id }}" value="1" {{ $constraint->multiple_checked_yes[$i] }}>
                        <span class="outer">
                        <span class="inner"></span></span>Yes
                    </label>
                    <label class="radio-label-preliminary radio-label {{ $data['disabled'] }}">
                        <input type="radio" class="radio-preliminary {{ $constraint->multiple_checked_no[$i] }}" name="radio-n-{{ $i . '-' . $constraint->id }}" value="0" data-id="{{ $constraint->id }}" {{ $constraint->multiple_checked_no[$i] }}>
                        <span class="outer">
                        <span class="inner"></span></span>No
                    </label>
                </div>
            @endfor
        @else
        <div class="item-question">
            <span>{{ $constraint->question }}</span>
        </div>
        <div class="item-answers {{ $data['cursor'] }}">
            <label class="radio-label-preliminary radio-label {{ $data['disabled'] }}">
                <input type="radio" class="radio-preliminary {{ $constraint->checked_yes }}" name="radio-y-{{ $constraint->id }}" data-id="{{ $constraint->id }}" value="1" {{ $constraint->checked_yes }}>
                <span class="outer">
                <span class="inner"></span></span>Yes
            </label>
            <label class="radio-label-preliminary radio-label {{ $data['disabled'] }}">
                <input type="radio" class="radio-preliminary {{ $constraint->checked_no }}" name="radio-n-{{ $constraint->id }}" data-id="{{ $constraint->id }}" value="0" {{ $constraint->checked_no }}>
                <span class="outer">
                <span class="inner"></span></span>No
            </label>
        </div>
        @endif

    </div>
@endforeach

@if ($data['preliminary_progress'])
    <div class="col-sm-10 offset-sm-1 col-lg-8 offset-lg-2">
        <p class="text-center">You have already answered the following questions. Please press the reset button, in order to change the answers. Pressing reset will also reset the GDPR Dashboard.</p>
        <button class="btn btn-info btn-submit btn-reset reset-preliminary" id="reset-preliminary">Reset</button>
    </div>
@else
    <input type="submit" class="btn btn-info btn-submit" id="submit-preliminary" value="Submit">
@endif

</form>

@endsection
