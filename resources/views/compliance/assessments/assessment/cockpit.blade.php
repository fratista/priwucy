@extends('compliance.layouts.assessment-old', ['data' => $data])

@section('title', 'GDPR Compliance Dashboard')
@section('information-title', 'This is the general dashboard. It will show an overview ov all available chapters.')

@php
if (!$data['preliminary_progress']) {
    echo '<meta http-equiv="Refresh" content="0;preliminary-check">';
    exit;
}

@endphp

@section('subpart')

<div class="xs-hidden col-sm-3"></div>
<div class="col-sm-6">
    <p class="text-xs-center dashboard-description">This is the GDPR compliance
        dashboard. It shows the
        applicable chapters, which
        contain questions regarding
        obligations defined in the GDPR.
        Obligations may have associated
        sub-obligations. The progress
        indicator shows the completion
        level for each chapter i.e.
        percentage of the answered
        questions.
    </p>
</div>
<div class="xs-hidden col-sm-3"></div>

@foreach ($data['chapters'] as $chapter)
    <div class="col-sm-6 col-md-4 col-lg-3 {{ $chapter->cursor }}">
        <div class="category {{ $chapter->disabled }}" id="category">
            <div class="pri-circle-wrapper">
                <div class="c100 p{{ round($chapter->status) }}">
                    <span class="pri-circle-progress">{{ $chapter->status }} %</span>
                     <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
            </div>
            <h3 class="category-title">{{ $chapter->title }}</h3>
            <button class="btn btn-info btn-submit show-chapter">
                <a class="btn-show-duties" href="/assessments/assessment/chapters/{{ App\Helper::extractName($chapter->uri) }}">Start</a>
            </button>
        </div>
    </div>
@endforeach

@if ($data['assessment_completion'] == 1)
    <div class="container last-dashboard">
        <p class="text-center">All questions have been answered and a report can now be generated.</p>
        <a href="/assessments/reports/report" class="btn btn-info btn-submit goto-report">Generate Report</a>
    </div>
@endif

@endsection
