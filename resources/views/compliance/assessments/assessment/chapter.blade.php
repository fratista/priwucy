@extends('compliance.layouts.assessment-old', ['data' => $data])

@section('title', $data['chapter']->title)
@section('information-title', 'This is the chapter overview.')

@php
if (!$data['preliminary_progress']) {
    echo '<meta http-equiv="Refresh" content="0;assessments/assessment/preliminary-check">';
    exit;
}
@endphp

@section('subpart')
    <input type="hidden" name="chapter-id" value="{{ $data['chapter']->id }}" />
    <div class="xs-hidden col-sm-3"></div>
    <div class="col-sm-6">
        <p class="text-xs-center dashboard-description">This is the chapter overview. All
            applicable duties and features are shown to be filled out. Depending on your
            answers, additional questions can popup.
        </p>
    </div>
    <div class="xs-hidden col-sm-3"></div>

    <form id="chapter-questions">

        @if ($data['chapter']->submission == 1)
            <div class="container col-sm-6">
                <p class="text-center">This chapter has already been completed. Below you can see the given answers. If you want to redo the survey, click on the reset button</p>
                <button class="btn btn-info btn-submit btn-reset reset-chapter">Reset</button>
            </div>
        @endif

        {{----- DUTIES -----}}

        @foreach ($data['duties'] as $duty)
        <div class="item-container duty-container @if ($data['chapter']->submission == 1) form-disabled @endif">
            <div class="item-title duty-title">
                <span>{{ $duty->duty_title }}</span>
                <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $duty->tooltip }}<br><br><p class=&#34;tooltip-paragraph&#34;>Paragraph: {{ $duty->paragraph_number }}</p>"></i>
            </div>
            <div class="item-question">
                <span>{{ $duty->duty_question }}</span>
            </div>
            <div class="item-answers">
                <input type="hidden" data-duty-id="{{ $duty->id }}">
                <div class="form-group">
                    <label class="radio-label">
                        <input class="duty-radio" id="duty{{ $duty->id  }}-yes" type="radio" name="duty{{ $duty->id }}" value="1"{{ $duty->radio['yes'] }}>
                        <span class="outer">
                        <span class="inner"></span></span>Yes
                    </label>
                    <label class="radio-label">
                        <input class="duty-radio" id="duty{{ $duty->id  }}-no" type="radio" name="duty{{ $duty->id }}" value="0"{{ $duty->radio['no'] }}>
                        <span class="outer">
                        <span class="inner"></span></span>No
                    </label>
                    <label class="radio-label">
                        <input class="duty-radio" id="duty{{ $duty->id  }}-unsure" type="radio" name="duty{{ $duty->id }}" value="2"{{ $duty->radio['unsure'] }}>
                        <span class="outer">
                        <span class="inner"></span></span>Not Sure
                    </label>
                </div>
                <div class="form-group">
                    <label class="label-comment">Comment</label>
                    <textarea class="form-control duty-comment" id="duty{{ $duty->id }}-comment" rows="3">{{ $duty->comment }}</textarea>
                </div>
                <div class="form-group">
                    <label class="label-attachment">Attachment</label>
                    <input type="file" class="duty-attachment" id="duty{{ $duty->id }}-attachment" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                    <div class="input-group">
                        <input type="text" class="form-control attachment-placeholder" id="duty{{ $duty->id }}-attachment-placeholder" placeholder="{{ $duty->attachment }}" disabled="">
                        <span class="group-span-filestyle input-group-btn" tabindex="0">
                            <label for="duty{{ $duty->id }}-attachment" class="btn btn-primary">
                                <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                <span class="buttonText">Select a File</span>
                            </label>
                        </span>
                    </div>
                </div>
            </div>

            <div class="item-dependencies pl-3">

                {{----- FEATURES -----}}

                @foreach ($duty->features as $feature)
                    <div class="item-container feature-container {{ $feature->container_class }} {{ $duty->feature_collapse }}">
                        <div class="item-title">
                            <span>{{ $feature->feature_title }}</span>
                            <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $feature->feature_information }}<br><br>Paragraph: {{ $feature->paragraph_number }}"></i>
                        </div>
                        <div class="item-question">
                            <span>{{ $feature->feature_question }}</span>
                        </div>
                        <div class="item-relation container">
                            <span>{!! $feature->chapter_relation !!}</span>
                        </div>
                        <div class="item-answers {{ $feature->cursor }}">
                            <input type="hidden" data-feature-id="{{ $feature->id }}">
                            <div class="form-group">
                                <label class="radio-label {{ $feature->disabled }}">
                                    <input class="feature-radio feature{{ $feature->id }}-yes" type="radio" name="feature{{ $feature->id }}-duty{{ $duty->id }}" value="1" {{ $feature->radio['yes']}}>
                                    <span class="outer">
                                    <span class="inner"></span></span>Yes
                                </label>
                                <label class="radio-label {{ $feature->disabled }}">
                                    <input class="feature-radio feature{{ $feature->id }}-no" type="radio" name="feature{{ $feature->id }}-duty{{ $duty->id }}" value="0" {{ $feature->radio['no'] }}>
                                    <span class="outer">
                                    <span class="inner"></span></span>No
                                </label>
                                <label class="radio-label {{ $feature->disabled }}">
                                    <input class="feature-radio feature{{ $feature->id }}-unsure" type="radio" name="feature{{ $feature->id }}-duty{{ $duty->id }}" value="2" {{ $feature->radio['unsure'] }}>
                                    <span class="outer">
                                    <span class="inner"></span></span>Not Sure
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="label-comment">Comment</label>
                                <textarea class="form-control feature-comment feature{{ $feature->id }}-comment {{ $feature->disabled }}" rows="3">{{ $feature->comment }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="label-attachment">Attachment</label>
                                <input type="file" class="feature-attachment feature{{ $feature->id }}-attachment" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                <div class="input-group {{ $feature->disabled }}">
                                    <input type="text" class="form-control attachment-placeholder" id="feature{{ $feature->id }}-attachment-placeholder" placeholder="{{ $feature->attachment }}" disabled="">
                                    <span class="group-span-filestyle input-group-btn" tabindex="0">
                                        <label for="feature{{ $feature->id }}-attachment" class="btn btn-primary">
                                            <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                            <span class="buttonText">Select a File</span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="item-dependencies pl-3">

                            {{----- RELATED FEATURES -----}}

                            @foreach ($feature->related_features as $related_feature)
                                <div class="item-container related-feature-container {{ $related_feature->container_class }} {{ $feature->collapse }}">
                                    <div class="item-title">
                                        <span>{{ $related_feature->related_feature_title }}</span>
                                        <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $related_feature->related_feature_information }}<br><br>Paragraph: {{ $feature->paragraph_number }}"></i>
                                    </div>
                                    <div class="item-question">
                                        <span>{{ $related_feature->related_feature_question }}</span>
                                    </div>
                                    <div class="item-relation container">
                                        <span>{!! $related_feature->chapter_relation !!}</span>
                                    </div>
                                    <div class="item-answers {{ $related_feature->cursor }}">
                                        <input type="hidden" data-related-feature-id="{{ $related_feature->id }}">
                                        <div class="form-group">
                                            <label class="radio-label {{ $related_feature->disabled }}">
                                                <input class="feature-radio related-feature-radio related-feature{{ $related_feature->id }}-yes" type="radio" name="related-feature{{ $related_feature->id }}-feature{{ $feature->id }}-duty{{ $duty->id }}" value="1" {{ $related_feature->radio['yes']}}>
                                                <span class="outer">
                                                <span class="inner"></span></span>Yes
                                            </label>
                                            <label class="radio-label {{ $related_feature->disabled }}">
                                                <input class="feature-radio related-feature-radio related-feature{{ $related_feature->id }}-no" type="radio" name="related-feature{{ $related_feature->id }}-feature{{ $feature->id }}-duty{{ $duty->id }}" value="0" {{ $related_feature->radio['no'] }}>
                                                <span class="outer">
                                                <span class="inner"></span></span>No
                                            </label>
                                            <label class="radio-label {{ $related_feature->disabled }}">
                                                <input class="feature-radio related-feature-radio related-feature{{ $related_feature->id }}-unsure" type="radio" name="related-feature{{ $related_feature->id }}-feature{{ $feature->id }}-duty{{ $duty->id }}" value="2" {{ $related_feature->radio['unsure'] }}>
                                                <span class="outer">
                                                <span class="inner"></span></span>Not Sure
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-comment">Comment</label>
                                            <textarea class="form-control related-feature-comment related-feature{{ $related_feature->id }}-comment {{ $related_feature->disabled }}" rows="3">{{ $related_feature->comment }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-attachment">Attachment</label>
                                            <input type="file" class="related-feature-attachment related-feature{{ $related_feature->id }}-attachment" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                            <div class="input-group {{ $related_feature->disabled }}">
                                                <input type="text" class="form-control attachment-placeholder" id="related-feature{{ $related_feature->id }}-attachment-placeholder" placeholder="{{ $related_feature->attachment }}" disabled="">
                                                <span class="group-span-filestyle input-group-btn" tabindex="0">
                                                    <label for="related-feature{{ $related_feature->id }}-attachment" class="btn btn-primary">
                                                        <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                                        <span class="buttonText">Select a File</span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endforeach

                {{----- DISPENSATIONS -----}}

                @foreach ($duty->dispensations as $dispensation)
                    <div class="item-container dispensation-container {{ $dispensation->container_class }} {{ $duty->dispensation_collapse }}">
                        <div class="item-title">
                            <span>{{ $dispensation->dispensation_title }}</span>
                            <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $dispensation->dispensation_information }}<br><br>Paragraph: {{ $dispensation->paragraph_number }}"></i>
                        </div>
                        <div class="item-question">
                            <span>{{ $dispensation->dispensation_question }}</span>
                        </div>
                        <div class="item-relation container">
                            <span>{!! $dispensation->chapter_relation !!}</span>
                        </div>
                        <div class="item-answers {{ $dispensation->cursor }}">
                            <input type="hidden" data-dispensation-id="{{ $dispensation->id }}">
                            <div class="form-group">
                                <label class="radio-label {{ $dispensation->disabled }}">
                                    <input class="dispensation-radio dispensation{{ $dispensation->id }}-yes" type="radio" name="dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="1" {{ $dispensation->radio['yes'] }}>
                                    <span class="outer">
                                    <span class="inner"></span></span>Yes
                                </label>
                                <label class="radio-label {{ $dispensation->disabled }}">
                                    <input class="dispensation-radio dispensation{{ $dispensation->id }}-no" type="radio" name="dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="0" {{ $dispensation->radio['no'] }}>
                                    <span class="outer">
                                    <span class="inner"></span></span>No
                                </label>
                                <label class="radio-label {{ $dispensation->disabled }}">
                                    <input class="dispensation-radio dispensation{{ $dispensation->id }}-unsure" type="radio" name="dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="2" {{ $dispensation->radio['unsure'] }}>
                                    <span class="outer">
                                    <span class="inner"></span></span>Not Sure
                                </label>
                            </div>
                            <div class="form-group">
                                <label class="label-comment">Comment</label>
                                <textarea class="form-control dispensation-comment dispensation{{ $dispensation->id }}-comment {{ $dispensation->disabled }}" rows="3">{{ $dispensation->comment }}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="label-attachment">Attachment</label>
                                <input type="file" class="dispensation-attachment dispensation{{ $dispensation->id }}-attachment" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                <div class="input-group {{ $dispensation->disabled }}">
                                    <input type="text" class="form-control attachment-placeholder" id="dispensation{{ $dispensation->id }}-attachment-placeholder" placeholder="{{ $dispensation->attachment }}" disabled="">
                                    <span class="group-span-filestyle input-group-btn" tabindex="0">
                                        <label for="dispensation{{ $dispensation->id }}-attachment" class="btn btn-primary">
                                            <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                            <span class="buttonText">Select a File</span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="item-dependencies pl-3">

                            {{----- DISPENSATION FEATURES -----}}

                            @foreach ($dispensation->features as $dispensation_feature)
                                <div class="item-container dispensation-feature-container {{ $dispensation_feature->container_class }} {{ $dispensation->collapse }}">
                                    <div class="item-title">
                                        <span>{{ $dispensation_feature->dispensation_feature_title }}</span>
                                        <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $dispensation_feature->dispensation_feature_information }}<br><br>Paragraph: {{ $dispensation_feature->paragraph_number }}"></i>
                                    </div>
                                    <div class="item-question">
                                        <span>{{ $dispensation_feature->dispensation_feature_question }}</span>
                                    </div>
                                    <div class="item-relation container">
                                        <span>{!! $dispensation_feature->chapter_relation !!}</span>
                                    </div>
                                    <div class="item-answers {{ $dispensation_feature->cursor }}">
                                        <input type="hidden" data-dispensation-feature-id="{{ $dispensation_feature->id }}">
                                        <div class="form-group">
                                            <label class="radio-label {{ $dispensation_feature->disabled }}">
                                                <input class="feature-radio dispensation-feature-radio dispensation-feature{{ $dispensation_feature->id }}-yes" type="radio" name="dispensation-feature{{ $dispensation_feature->id }}-dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="1" {{ $dispensation_feature->radio['yes'] }}>
                                                <span class="outer">
                                                <span class="inner"></span></span>Yes
                                            </label>
                                            <label class="radio-label {{ $dispensation_feature->disabled }}">
                                                <input class="feature-radio dispensation-feature-radio dispensation-feature{{ $dispensation_feature->id }}-no" type="radio" name="dispensation-feature{{ $dispensation_feature->id }}-dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="0" {{ $dispensation_feature->radio['no'] }}>
                                                <span class="outer">
                                                <span class="inner"></span></span>No
                                            </label>
                                            <label class="radio-label {{ $dispensation_feature->disabled }}">
                                                <input class="feature-radio dispensation-feature-radio dispensation-feature{{ $dispensation_feature->id }}-unsure" type="radio" name="dispensation-feature{{ $dispensation_feature->id }}-dispensation{{ $dispensation->id }}-duty{{ $duty->id }}" value="2" {{ $dispensation_feature->radio['unsure'] }}>
                                                <span class="outer">
                                                <span class="inner"></span></span>Not Sure
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-comment">Comment</label>
                                            <textarea class="form-control dispensation-feature-comment dispensation-feature{{ $dispensation_feature->id }}-comment {{ $dispensation_feature->disabled }}" rows="3">{{ $dispensation_feature->comment }}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-attachment">Attachment</label>
                                            <input type="file" class="dispensation-feature-attachment dispensation-feature{{ $feature->id }}-attachment" tabindex="-1" style="position: absolute; clip: rect(0px, 0px, 0px, 0px);">
                                            <div class="input-group {{ $dispensation_feature->disabled }}">
                                                <input type="text" class="form-control attachment-placeholder" id="dispensation-feature{{ $dispensation_feature->id }}-attachment-placeholder" placeholder="{{ $dispensation_feature->attachment }}" disabled="">
                                                <span class="group-span-filestyle input-group-btn" tabindex="0">
                                                    <label for="dispensation-feature{{ $dispensation_feature->id }}-attachment" class="btn btn-primary">
                                                        <span class="icon-span-filestyle glyphicon glyphicon-folder-open"></span>
                                                        <span class="buttonText">Select a File</span>
                                                    </label>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                @endforeach

            </div>
        </div>
        @endforeach

        @if ($data['chapter']->submission == 1)
            <div class="container col-sm-6">
                <p class="text-center">This chapter has already been completed. Below you can see the given answers. If you want to redo the survey, click on the reset button</p>
                <button class="btn btn-info btn-submit btn-reset reset-chapter">Reset</button>
            </div>
        @else
            <input type="submit" class="btn btn-info btn-submit" id="submit-chapter" value="Submit">
        @endif

    </form>
@endsection
