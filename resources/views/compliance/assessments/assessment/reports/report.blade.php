@extends('compliance.layouts.assessment-old', ['data' => $data])

@section('title', 'Report')
@section('information-title', 'This is the report. It shows an overview of all applicable chapters and their stats.')

@section('subpart')

<div class="container col-sm-6 mb-4">
    <p class="text-center">This is the report section. It will provide an overview of all chapters with the answers to the applicable obligations and the degree of compliance with the GDPR. The circle indicates the percentage.</p>
</div>

<div class="focus-wrapper report-hidden container">
    <div class="col-sm-10 mx-auto">
        <div class="row">
            <div class="col-md-8 col-lg-12 mx-auto">
                <h2 class="report-heading">Individual Statistics
                    <i class="fa fa-close" id="hide-details" aria-hidden="true"></i>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col chapter-focus-body"></div>
        </div>
    </div>
</div>

<div class="stats-wrapper container">
    <!--
    <div class="row">
        <div class="col-md-8 col-lg-12 mx-auto">
            <h2 class="report-heading">General Statistics</h2>
        </div>
    </div>
    <div class="row">
        <div class="hidden-sm-down col-sm-2 hidden-lg-up"></div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="question-container stats-container">
                <div class="container separation-title">Preliminaries</div>
                <div class="pri-report-progress-wrapper">
                    <div class="c100 p<?php //echo $preliminaryIndicator; ?>">
                        <span class="pri-circle-progress"><?php //echo $preliminaryCount; ?></span>
                         <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="question-container stats-container">
                <div class="container separation-title">Chapters</div>
                <div class="pri-report-progress-wrapper">
                    <div class="c100 p100">
                        <span class="pri-circle-progress"><?php //echo $regulation->numChapters; ?></span>
                         <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-sm-down col-sm-2 hidden-lg-up"></div>
        <div class="hidden-sm-down col-sm-2 hidden-lg-up"></div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="question-container stats-container">
                <div class="container separation-title">Obligations</div>
                <div class="pri-report-progress-wrapper">
                    <div class="c100 p<?php //echo $dutyIndicator; ?>">
                        <span class="pri-circle-progress"><?php //echo $answeredDuties ?></span>
                         <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="question-container stats-container">
                <div class="container separation-title">Sub-obligations</div>
                <div class="pri-report-progress-wrapper">
                    <div class="c100 p<?php //echo $featureIndicator; ?>">
                        <span class="pri-circle-progress"><?php //echo $answeredFeatures; ?></span>
                         <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-sm-down col-sm-2 hidden-lg-up"></div>
    </div>
    -->

    <div class="row mb-3">
        <div class="col-xs-12 col-lg-9 col-xl-8 mb-2">
            <div class="row pri-legend-wrapper pl-3 pr-3">
                <div class="pri-legend-element">
                    <span class="pri-legend-square green"></span>
                    <p class="pri-legend-term">Fulfilled</p>
                </div>
                <div class="pri-legend-element">
                    <span class="pri-legend-square red"></span>
                    <p class="pri-legend-term">Unfulfilled</p>
                </div>
                <div class="pri-legend-element">
                    <span class="pri-legend-square orange"></span>
                    <p class="pri-legend-term">Recommended</p>
                </div>
                <div class="pri-legend-element">
                    <span class="pri-legend-square darkgrey"></span>
                    <p class="pri-legend-term">Obligation</p>
                </div>
                <div class="pri-legend-element">
                    <span class="pri-legend-square grey"></span>
                    <p class="pri-legend-term">Sub-obligation</p>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-end col-xs-12 col-lg-3 col-xl-4">
            <span class="pri-toggle-name">Detailed View</span>
            <input type="checkbox" id="unchecked" class="pri-toggle-checkbox collapse" />
            <label for="unchecked" class="pri-toggle-label"></label>
        </div>
    </div>
</div>

<div class="chapter-wrapper container">
    <div class="row">
        <div class="col-sm-8 col-md-12 mx-auto">
            <h2 class="report-heading">Chapter Statistics</h2>
        </div>
    </div>
    <div class="row">

        @foreach ($data['report']->data['chapters'] as $chapter)
            @if (1)
                <div class="chapter-container col-sm-8 col-md-6 col-lg-4 offset-sm-2 offset-md-0">
                    <div class="chapter-section" data-chapter-name="chapter-{{ $chapter->id }}">
                        <div class="chapter-title">{{ $chapter->title }}</div>
                        <div class="chapter-body">
                            <div class="chapter-stats">
                                <div class="row chapter-total">
                                    <div class="col">
                                        <div class="question-container stats-container">
                                            <div class="container separation-title">Completion</div>
                                            <div class="pri-report-progress-wrapper">
                                                <div class="c100 p{{ round($chapter->fulfilledTotalStatus) }}">
                                                    <span class="pri-circle-progress">{{ $chapter->applicableTotalCount }}</span>
                                                     <div class="slice">
                                                        <div class="bar"></div>
                                                        <div class="fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row chapter-split collapse">
                                    <div class="col-sm-6">
                                        <div class="question-container stats-container">
                                            <div class="container separation-title">Obligations</div>
                                            <div class="pri-report-progress-wrapper">
                                                <div class="c100 p{{ round($chapter->fulfilledDutyStatus) }}">
                                                    <span class="pri-circle-progress">{{ $chapter->applicableDutyCount }}</span>
                                                     <div class="slice">
                                                        <div class="bar"></div>
                                                        <div class="fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="question-container stats-container">
                                            <div class="container separation-title">Sub-obligations</div>
                                            <div class="pri-report-progress-wrapper">
                                                <div class="c100 p{{ round($chapter->fulfilledFeatureStatus) }}">
                                                    <span class="pri-circle-progress">{{ $chapter->featureCount }}</span>
                                                     <div class="slice">
                                                        <div class="bar"></div>
                                                        <div class="fill"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <button class="btn btn-info btn-submit" id="view-details">View Details</button>
                                </div>
                            </div>
                            <div class="chapter-detail">
                                <div class="fulfilled-container">
                                    <div class="chapter-title">Fulfilled Obligations
                                    <i class="fa fa-caret-down" id="expand-fulfilled" aria-hidden="true"></i>
                                    </div>

                                    @foreach ($chapter->fulfilled as $fulfilled)
                                        <div class="report-item-container duty-container report-hidden">
                                            <div class="item-title duty-title">{{ $fulfilled->duty_title }}
                                                <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $fulfilled->paragraph_number }}<br>"></i>
                                            </div>
                                            <div class="item-question container">{{ $fulfilled->duty_question }}</div>
                                            <div class="item-paragraph container">Paragraph: {{ $fulfilled->paragraph_number }}</div>
                                            <div class="item-dependencies container">

                                                @foreach ($fulfilled->features as $feature)
                                                    <div class="feature-container">
                                                        <div class="item-title feature-title">{{ $feature->feature_title}}
                                                            <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $feature->feature_information }}<br>"></i>
                                                        </div>
                                                        <div class="item-question container">{{ $feature->feature_question }}</div>
                                                        <div class="item-paragraph container">Paragraph: {{ $feature->paragraph_number }}</div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                <div class="unfulfilled-container">
                                    <div class="chapter-title">Unfulfilled Obligations
                                        <i class="fa fa-caret-down" id="expand-unfulfilled" aria-hidden="true"></i>
                                    </div>

                                    @foreach ($chapter->critical as $critical)
                                        <div class="report-item-container duty-container report-hidden">
                                            <div class="item-title duty-title">{{ $critical->duty_title }}
                                                <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $critical->paragraph_number }}<br>"></i>
                                            </div>
                                            <div class="item-question container">{{ $critical->duty_question }}</div>
                                            <div class="item-paragraph container">Paragraph: {{ $critical->paragraph_number }}</div>
                                            <div class="item-dependencies container">

                                                @foreach ($critical->features as $feature)
                                                    <div class="feature-container {{ $feature->color }}">
                                                        <div class="item-title feature-title">{{ $feature->feature_title }}
                                                            <i class="fa fa-info" aria-hidden="true" data-html="true" data-toggle="tooltip" data-placement="top" title="{{ $feature->feature_information }}<br>"></i>
                                                        </div>
                                                        <div class="item-question container">{{ $feature->feature_question }}</div>
                                                        <div class="item-paragraph container">Paragraph: {{ $feature->paragraph_number }}</div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                <div class="not-applicable-container">
                                    <div class="chapter-title">Not Applicable Obligations
                                        <i class="fa fa-caret-down" id="expand-not-applicable" aria-hidden="true"></i>
                                    </div>
                                    <!-- unsure container -->
                                </div>
                            </div>
                            <div class="chapter-print">
                                <p class="text-xs-center" style="text-align:center">The report can also be downloaded as PDF file. Please click here for download.</p>
                                <button class="btn btn-info btn-submit" id="download-report">Download Chapter Report</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

    </div>
</div>

<div class="download-wrapper container">
    <p class="text-xs-center" style="text-align:center">The report can also be downloaded as PDF. Please use the button to select the chapters to generate a report.</p>
    <div class="report-button-container">
        <div class="row">
            <button class="btn btn-info btn-submit btn-download" data-action="generate-report">Generate Report</button>
        </div>
        <div class="row">
            <div class="col-3 mx-auto">
                <button class="btn btn-info btn-submit d-none" data-action="select-all-report">Select All</button>
                <button class="btn btn-info btn-submit d-none" data-action="cancel-report">Cancel</button>
                <button class="btn btn-info btn-submit d-none" data-action="download-report">Download Report</button>
            </div>
        </div>
    </div>
</div>

@endsection
