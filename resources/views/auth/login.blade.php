@extends('layouts.fullscreen')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-6 col-xl-5 mx-auto">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-field text-center">
                        <img src="{{ asset('img/logo-inverted.png') }}" alt="" class="responsive-img valign profile-image-login">
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal form-auth" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col mb-5 mt-5">
                            <i class="fa fa-envelope"></i><input id="email" type="email" class="form-control login" name="email" value="{{ old('email') }}" required autofocus>
                                <label for="email" class="control-label">E-Mail Address</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col">
                                <i class="fa fa-key"></i>
                                <input id="password" type="password" class="form-control login" name="password" required>
                                <label for="password" class="control-label">Password</label>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col pt-4">
                                <div class="checkbox">
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label>Remember Me</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col">
                                <button type="submit" class="btn btn-primary btn-block mt-4">Login</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <a class="btn btn-link" href="{{ route('register') }}">Register now!</a>
                            <a class="btn btn-link float-right" href="{{ route('password.request') }}">Forgot Your Password?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
