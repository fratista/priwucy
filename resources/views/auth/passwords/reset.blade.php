@extends('layouts.fullscreen')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-6 col-xl-5 mx-auto">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-field text-center">
                        <img src="{{ asset('img/logo-inverted.png') }}" alt="" class="responsive-img valign profile-image-login">
                    </div>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col">
                                <input id="email" type="email" class="form-control login" name="email" value="{{ $email or old('email') }}" required autofocus>
                                <label for="email" class="control-label">E-Mail Address</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col">
                                <input id="password" type="password" class="form-control login" name="password" required>
                                <label for="password" class="control-label">Password</label>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="col">
                                <input id="password-confirm" type="password" class="form-control login" name="password_confirmation" required>
                                <label for="password-confirm" class="control-label">Confirm Password</label>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col">
                                <button type="submit" class="btn btn-primary btn-block mt-4">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
