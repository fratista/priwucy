@extends('layouts.fullscreen')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-lg-6 col-xl-5 mx-auto">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="input-field text-center">
                        <img src="{{ asset('img/logo-inverted.png') }}" alt="" class="responsive-img valign profile-image-login">
                    </div>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col">
                                <input id="email" type="email" class="form-control login" name="email" value="{{ old('email') }}" required>
                                <label for="email" class="control-label">E-Mail Address</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col">
                                <button type="submit" class="btn btn-primary btn-block mt-4">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
