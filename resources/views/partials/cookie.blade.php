<?php if (!isset($_COOKIE['disclaimer'])){ ?>
<div class="cookie-bar">
    <div class="container">
        <p>We use cookies to enhance your experience on our web site. By visiting it, you agree our
            <a href="#" class="cookie-policy">Cookies Policy</a>
            <a href="#" class="btn btn-info" id="accept-cookie">I Understand</a>
        </p>
    </div>
</div>
<?php }?>
