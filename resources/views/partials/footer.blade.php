<footer class="bd-footer text-muted mt-4">
    <div class="container pb-4">
        <div class="row">
            <div class="col-sm-6 col-lg-3 mt-4">
                <h4>Sites</h4>
                <ul>
                    <li><a href="/index.php">Home</a></li>
                    <li><a href="/assessment.php">Assessment</a></li>
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Register</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 mt-4">
                <h4>Resources</h4>
                <ul>
                    <li><a href="#">Terminology</a></li>
                    <li><a href="#">Data Protection Regulation</a></li>
                    <li><a href="#">Privacy Lab</a></li>
                    <li><a href="#">EUR Lex</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 mt-4">
                <h4>About</h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">What is Priwucy</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Elements</a></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 mt-4">
                <h4>Support</h4>
                <ul>
                    <li><a href="#">Help Center</a></li>
                    <li><a href="#">Status</a></li>
                    <li><a href="#">Premium Support</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="sub-footer pb-3">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-6">
                    <ul class="d-block d-sm-flex mt-3">
                        <li><a href="https://www.wu.ac.at">www.wu.ac.at</a></li>
                        <li><a href="#">Terms of Service</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Cookies</a></li>
                        <li class="copyright">© 2017 WU Vienna</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
