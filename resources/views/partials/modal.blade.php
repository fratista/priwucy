<!-- fade show -->
<div id="modal-confirm" class="modal confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLiveLabel" style="display: block; padding-right: 17px;">
  <div class="modal-container modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLiveLabel">Do you want to proceed?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>When proceeding, you cannot return back</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modal-submit-preliminary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal loading hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-container loading">
        <div class="modal-header">
            <h1 class="pri-loading-title">Processing...</h1>
        </div>
        <div class="modal-body">
            <h4 class="pri-loading-message text-center">Fetching data</h4>
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated w-100" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
        </div>
    </div>
</div>
