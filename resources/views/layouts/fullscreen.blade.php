@extends('skeleton')

@section('layout')

<div class="full-screen">
    
    @yield('content')

</div>

@endsection
