@extends('layouts.site')

@section('content')
<header class="header light-blue">
    <div class="container">
        <div class="intro">
            <h1 class="intro-h1">Ready for the GDPR?</h1>
            <h2 class="intro-h2">Conduct your self-assessment of compliance with the General Data Protection Regulation</h2>
            <a href="assessment/preliminary-check/" class="btn btn-outline-secondary white-inverted" id="start-assessment">Start Assessment</a>
        </div>
    </div>
</header>

<div class="container col-md-6 pt-4 mt-4 mb-4">
    <h3 class="pri-section-heading text-center">What changes does the new GDPR bring for data processing in The European Union?</h2>
</div>

<div class="container pt-3">
  <div class="row">
    <div class="pt-4 teaser-container">
        <div class="col-sm-5">
            <h3 class="text-center mb-3 font15">Higher standards for transparency</h3>
            <p class="text-center">
                The GDPR defines strict guidelines to ensure transparency associated with the data processing
            </p>
        </div>
        <div class="col-sm-7">
            <img src="img/teaser/teaser2.jpg" class="img-fluid" alt="Feedback">
        </div>
    </div>
    <div class="pt-4 teaser-container">
        <div class="col-sm-7">
            <img src="img/teaser/teaser6.jpg" class="img-fluid" alt="Feedback">
        </div>
         <div class="col-sm-5">
            <h3 class="text-center mb-3 font15">Provision to delete personal information</h3>
            <p class="text-center">
                The customer can now request your organisation to delete their personal records based on the new "Right to be Forgotten"
            </p>
        </div>
    </div>
    <div class="pt-4 teaser-container">
        <div class="col-sm-5">
            <h3 class="text-center mb-3 font15">Provision for data portability</h3>
            <p class="text-center">
                Organisations are now obliged to provide a copy of personal data to customers in a structured and commonly used format, if requested
            </p>
        </div>
        <div class="col-sm-7">
            <img src="img/teaser/teaser1.jpg" class="img-fluid" alt="Privacy">
        </div>
    </div>
    <div class="pt-4 teaser-container">
        <div class="col-sm-7">
            <img src="img/teaser/teaser3.jpg" class="img-fluid" alt="Saveable">
        </div>
        <div class="col-sm-5">
            <h3 class="text-center mb-3 font15">Provisions for child protection</h3>
            <p class="text-center">
                The GDPR requires organisations to obtain consent from guardians of children under 16 years before processing their data
            </p>
        </div>
    </div>
    <div class="pt-4 teaser-container">
        <div class="col-sm-5">
            <h3 class="text-center mb-3 font15">Bigger Sanctions</h3>
            <p class="text-center">
                When infringing the regulation, the organisation at fault can be fined up to 20.000.000 € or 4% of its annual worldwide turnover
            </p>
        </div>
        <div class="col-sm-7">
            <img src="img/teaser/teaser5.jpg" class="img-fluid" alt="Feedback">
        </div>
    </div>
    <!--
    <div class="col-md-12 pt-4 teaser-container">
        <div class="col-sm-5">
            <h3 class="text-center mt-1 font15 text-uppercase">One-stop-shop</h3>
            <p class="text-center">
                In case of problems with how my data is handled, I can contact my national data protection authority, whatever the country where the organisation is processing my data
            </p>
        </div>
        <div class="col-sm-7">
            <img src="img/teaser/teaser4.jpg" class="img-resize-center" alt="Feedback">
        </div>
    </div>
    -->
  </div>
</div>
@endsection
