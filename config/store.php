<?php

return [

    /*
    |--------------------------------------------------------------------------
    | ARC2 Store configuration for RDF endpoint
    |--------------------------------------------------------------------------
    |
    | Here lies the connection setup the SPARQL queries.
    | The queries  are making use of the Semsol ARC2 package
    | for which the RDF endpoint is configured.
    |
    */
    'db_host' => env('DB_HOST', 'localhost'),
    'db_name' => 'priwucy',
    'db_user' => env('DB_USERNAME', 'root'),
    'db_pwd' => env('DB_PASSWORD', ''),
    'store_name' => 'my_store',

];
