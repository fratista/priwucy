<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Assessment settings
    |--------------------------------------------------------------------------
    |
    | Set the mode of the answering process for the assessment.
    | If this is set to individual, each duplicate question will
    | be answered invividually and also targeted as that in the report.
    |
    */

    'answer_mode' => [
        'global',  /* 'individual',*/
    ],

];
